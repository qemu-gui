#include "notebook.h"
#include "console.h"
#include "vncpanel.h"
#include "vm.h"
#include <wx/wx.h>

DEFINE_EVENT_TYPE(EVT_NEW_VM)
DEFINE_EVENT_TYPE(EVT_OPEN_VM)

WelcomeHtmlWindow::WelcomeHtmlWindow(wxWindow *parent)
                  :wxHtmlWindow(parent){
	this->LoadPage(wxT("welcome.html"));
}

void WelcomeHtmlWindow::OnLinkClicked(const wxHtmlLinkInfo& link){
	const wxString href = link.GetHref();
	
	if(!href.CmpNoCase(wxT("/vm/new"))){
		wxCommandEvent event(EVT_NEW_VM,wxID_ANY);
		ProcessEvent(event);
		return;
	}
	
	if(!href.CmpNoCase(wxT("/vm/open"))){
		wxCommandEvent event(EVT_OPEN_VM,wxID_ANY);
		ProcessEvent(event);
		return;
	}

	LoadPage(href);
}

QemuUINotebookPage::QemuUINotebookPage(wxWindow *parent,QemuVM *vm,wxPanel *info,VNCPanel *vncpanel,ConsolePanel *console)
                   :wxPanel(parent,wxID_ANY),
                    vm(vm),
		    info(info),
		    vncpanel(vncpanel),
		    console(console),
                    active(NULL){

	sizer = new wxBoxSizer(wxVERTICAL);
#if 0
	if(vncpanel)
		sizer->Add(vncpanel,1,wxEXPAND);
	if(console)
		sizer->Add(console,1,wxEXPAND);
#endif
	//SetAutoLayout(true);
	SetSizer(sizer);
}

QemuVM* QemuUINotebookPage::GetVM(){
	return this->vm;
}

void QemuUINotebookPage::SetVM(QemuVM *vm){
	this->vm = vm;
}

void QemuUINotebookPage::ShowInfo(){
	active = info;
}

bool QemuUINotebookPage::IsInfoPageActive(){
	return active == info;
}

VNCPanel* QemuUINotebookPage::GetScreen(){
	return this->vncpanel;
}

void QemuUINotebookPage::SetVNCPanel(VNCPanel *vncpanel){
	this->vncpanel = vncpanel;
	sizer->Add(vncpanel,1,wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL,0);
	sizer->Layout();
}

bool QemuUINotebookPage::IsScreenActive(){
	return active == vncpanel;
}

ConsolePanel* QemuUINotebookPage::GetConsole(){
	return this->console;
}

void QemuUINotebookPage::SetConsole(ConsolePanel *console){
	this->console = console;
	GetSizer()->Add(console,1,wxEXPAND,0);
	GetSizer()->Layout();
}

bool QemuUINotebookPage::IsConsoleActive(){
	return active == console;
}

void QemuUINotebookPage::ShowScreen(){
	this->console->Show(false);
	this->vncpanel->Show(true);
	GetSizer()->Layout();
	active = vncpanel;
}

void QemuUINotebookPage::ShowConsole(){
	this->vncpanel->Show(false);
	this->console->Show(true);
	GetSizer()->Layout();
	active = console;
}

QemuUINotebook::QemuUINotebook(wxWindow *parent) 
               :wxNotebook(parent,QEMU_VM_NOTEBOOK,wxDefaultPosition,
                           wxDefaultSize,wxNB_TOP)
{
	this->homepage = NULL;
};

void QemuUINotebook::ShowContextMenu(const wxPoint& pos)
{
	wxMenu menu;
	menu.Append(QEMU_VM_NOTEBOOK_CONTEXT_CLOSE, _("C&lose"));
	PopupMenu(&menu, pos);
}

void QemuUINotebook::OnMouseEvent(wxMouseEvent& event)
{
    wxEventType eventType = event.GetEventType();

    static int lastTabID = -1;
	
	if(eventType==wxEVT_LEFT_DOWN){
		// Save the selected tab
		lastTabID=HitTest(wxPoint(event.GetX(),event.GetY())); 
		wxSetCursor(wxCursor(wxCURSOR_HAND));
	} else if(eventType == wxEVT_RIGHT_DOWN){
		this->contextTabID = HitTest(event.GetPosition());
		ShowContextMenu(event.GetPosition());
		lastTabID=-1;
	} else if(eventType==wxEVT_LEFT_UP && lastTabID >= 0){
		int tabID = HitTest(wxPoint(event.GetX(), event.GetY()));
		//std::cout << "OLD: " << lastTabID << " NEW: " << tabID << "\n";
		if(tabID >= 0 && tabID != lastTabID){
			wxNotebookPage* page=GetPage(lastTabID);
			wxString text = GetPageText(lastTabID);
			int image = GetPageImage(lastTabID);
			RemovePage(lastTabID);
			InsertPage(tabID, page, text, true, image);
			// update window menu
		}
		lastTabID=-1;
		wxSetCursor(wxCursor(wxCURSOR_ARROW));
	} else {
		lastTabID = -1;
	}
	
	event.Skip();
} 

void QemuUINotebook::CreateHomepage(){
	if(this->homepage)
		return;
	
	this->homepage = new WelcomeHtmlWindow(this);
	this->InsertPage(0,homepage,_("Welcome"),true);
}


int QemuUINotebook::ShowHomepage(){
	// if there is no homepage create it
	if(!homepage){
		CreateHomepage();
		return 0;
	}

	for(int i = 0; i < GetPageCount(); i++){
		if(homepage == GetPage(i)){
			SetSelection(i);
			return i;
		}
	}

	return 0;
}

void QemuUINotebook::CloseHomepage(){
	if(!homepage)
		return;
	
	for(int i = 0; i < GetPageCount(); i++){
		if(homepage == GetPage(i)){
			RemovePage(i);
			delete homepage;
			homepage = NULL;
			return;
		}
	}
}

bool QemuUINotebook::IsHomepage(const int pos){
	wxNotebookPage *page = this->GetPage(pos);
	return page == this->homepage;
}

bool QemuUINotebook::HasHomepage(){
	return homepage != NULL;
}

void QemuUINotebook::AddVM(QemuVM *vm){
	QemuUINotebookPage *page = new QemuUINotebookPage(this,vm);
	VNCPanel *vncpanel = new VNCPanel(page,wxString(wxT("127.0.0.1")),1);
	ConsolePanel *console = new ConsolePanel(page,vm);
	page->SetVNCPanel(vncpanel);
	page->SetConsole(console);
	page->ShowConsole();
	AddPage(page,vm->GetTitle());
	SetSelection(GetPageCount() - 1);
}

QemuUINotebookPage* QemuUINotebook::GetCurrentVMTab(){
	const int sel = GetSelection();
	if(sel == -1)
		return NULL;
	if(IsHomepage(sel))
		return NULL;
	
	return (QemuUINotebookPage *)GetCurrentPage();
}


QemuVM* QemuUINotebook::GetCurrentVM(){
	QemuUINotebookPage *tab = GetCurrentVMTab();
	if(tab)
		return tab->GetVM();
	else
		return NULL;
}

BEGIN_EVENT_TABLE(QemuUINotebook,wxNotebook)
	EVT_MOUSE_EVENTS(QemuUINotebook::OnMouseEvent)
END_EVENT_TABLE()
