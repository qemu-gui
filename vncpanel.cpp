#include "debug.h"
#include "vncpanel.h"
#include "vncdisplaythread.h"
#include "pipedprocess.h"
#include <wx/rawbmp.h>
#include <wx/sizer.h>

typedef wxAlphaPixelData PixelData;
//typedef wxPixelFormat<unsigned char, 32, 0, 1, 2> PixelData;

DEFINE_EVENT_TYPE(EVT_VNC_UPDATE)
DEFINE_EVENT_TYPE(EVT_VNC_RESIZE)

BEGIN_EVENT_TABLE(VNCPanel, wxScrolledWindow)
	EVT_PAINT(VNCPanel::OnPaint)
	EVT_ERASE_BACKGROUND(VNCPanel::OnEraseBackground)
	EVT_MOUSE_EVENTS(VNCPanel::OnMouseEvent)
	EVT_KEY_DOWN(VNCPanel::OnKeyDown)
	EVT_KEY_UP(VNCPanel::OnKeyUp)
	EVT_COMMAND(wxID_ANY,EVT_PROCESS_TERMINATED,VNCPanel::HandleProcessTermination)
END_EVENT_TABLE()

void dummy(){};

extern "C" {
	rfbBool VNCResizeCallback(rfbClient *client);
	void VNCUpdateCallback(rfbClient* cl,int x,int y,int w,int h);
}

VNCPanel::VNCPanel(wxWindow *parent,wxString host,int display)
         :wxScrolledWindow(parent, wxID_ANY),
                           vncclient(NULL),
			   vncdisplaythread(NULL),
			   locked(false){
}

VNCPanel::~VNCPanel(){
	Disconnect();
}

bool VNCPanel::Connect(int display){
	DEBUG_VNC("connecting to display: %d\n",display);
	return Connect(wxT(""),display);
}

bool VNCPanel::Connect(wxString host,int display){
	// 16-bit: vncclient=rfbGetClient(5,3,2); 
	// 24-bit?: vncclient = rfbGetClient(8,3,3);
	// 32-bit?:vncclient = rfbGetClient(8,3,4);
	vncclient = rfbGetClient(8,3,4);
	// store this instance so we can later use it within the callbacks
	rfbClientSetClientData(vncclient, (void *)dummy, this);
	vncclient->serverHost = strdup("127.0.0.1");
	vncclient->serverPort = display;
	vncclient->MallocFrameBuffer = VNCResizeCallback;
	vncclient->canHandleNewFBSize = true;
	vncclient->GotFrameBufferUpdate = VNCUpdateCallback;
/*
	vncclient->format.redShift = 16;
	vncclient->format.greenShift = 8;
	vncclient->format.blueShift = 0;
*/

	vncclient->appData.useBGR233 = 0;
	vncclient->appData.encodingsString = "copyrect hextile raw";
//	vncclient->appData.encodingsString = "tight zrle ultra copyrect hextile zlib corre rre raw";
	vncclient->appData.compressLevel = 0;
	vncclient->appData.qualityLevel = 9;

        if(vncclient->serverPort>=0 && vncclient->serverPort<5900)
		vncclient->serverPort+=5900;

	if(!rfbInitClient(vncclient,NULL,NULL)){
		DEBUG_VNC("Could not initialize vncclient.\n");
		vncclient = NULL;
		return false;
	}

	vncdisplaythread = new VNCDisplayThread(this);
	vncdisplaythread->Create();
	vncdisplaythread->Run();
	
	return true;
}

void VNCPanel::Disconnect(){
	rfbClient *tmp = vncclient;
	vncclient = NULL;
	if(vncdisplaythread != NULL){
		vncdisplaythread->Stop();
		vncdisplaythread->Wait();
		delete vncdisplaythread;
		vncdisplaythread = NULL;
	}
	
	if(tmp != NULL){
		free(tmp->frameBuffer);
		rfbClientCleanup(tmp);
	}
}

void VNCPanel::HandleProcessTermination(wxCommandEvent& WXUNUSED(event)){
	Disconnect();
}

rfbClient* VNCPanel::GetVNCClient(){
	return this->vncclient;
}

void VNCPanel::VNCUpdateRect(int x,int y,int w,int h){
	wxBitmap *bmp = VNCGenerateBitmap(x,y,w,h);
	if(!bmp)
		return;
	wxClientDC dc(this);
	dc.DrawBitmap(*bmp,x,y,false);
	delete bmp;
}

wxBitmap* VNCPanel::VNCGenerateBitmap(int x,int y,int w,int h){
	// can't draw if the frame buffer doesn't exists yet
	if(!vncclient || !vncclient->frameBuffer)
		return NULL;

	DEBUG_PAINT("generate bitmap: x:%d y:%d w:%d h:%d\n",x,y,w,h);
	// create a bitmap from our pixel data
//	wxBitmap *test = new wxBitmap((const char *)vncclient->frameBuffer,w,h,vncclient->format.bitsPerPixel);
//	return test;	
	wxBitmap *bmp = new wxBitmap(w,h,vncclient->format.bitsPerPixel);

        PixelData data(*bmp);
	if(!data){
	     DEBUG_PAINT("could not access raw pixel data\n");
	     return NULL;
	}

	data.UseAlpha();
	PixelData::Iterator p(data);
//	unsigned char *rawdata=(unsigned char *)&p.Data();
//	unsigned char *rawdata=(unsigned char *)p.m_ptr;

	//p.m_ptr = vncclient->frameBuffer;
	//return bmp;

	// uint8_t *pp = (uint8_t *)&it.Data();
	uint8_t *px = (uint8_t*)vncclient->frameBuffer;
	// move the pointer to the beginning of the requested area
	px+=((vncclient->width * y + x) * 4);
	int offsetx = ((vncclient->width - w)*4);

	for( int cy = 0; cy < h; ++cy ){
		PixelData::Iterator rowStart = p;
		for( int cx = 0; cx < w; ++cx ){
			p.Red()   = px[0];
			p.Green() = px[1];
			p.Blue()  = px[2];
			p.Alpha() = 255;
			px+=4;
                	++p; // same as p.OffsetX(1)
		}
		px+=offsetx;
		p = rowStart;
		p.OffsetY(data, 1);
        }
	
	return bmp;
}

rfbBool VNCPanel::VNCResize(){
	if(!vncclient)
		return false;

	DEBUG_VNC("resize: w: %d h: %d\n",vncclient->width,vncclient->height);
	if(vncclient->frameBuffer)
		free(vncclient->frameBuffer);
	
	vncclient->frameBuffer = (uint8_t*)malloc(
		vncclient->width * vncclient->height *
		vncclient->format.bitsPerPixel/8
	);

	if(vncclient->frameBuffer){
		wxSize size(vncclient->width,vncclient->height);
		SetSize(size);
		SetMinSize(size);
		SetMaxSize(size);
		GetParent()->GetSizer()->Layout();
		DEBUG_VNC("resize succeeded\n");
		Paint();
		return true;	
	}
	DEBUG_VNC("resize failed\n");
	return false;
}

void VNCPanel::OnPaint(wxPaintEvent& WXUNUSED(event)){
	if(!vncclient)
		return;
	
	wxPaintDC dc(this);
	int x,y,w,h;
	wxRegionIterator upd(GetUpdateRegion());

	while(upd){
		x = upd.GetX();
		y = upd.GetY();
		w = upd.GetW();
		h = upd.GetH();
		// it is important that those values are never bigger than
		// our framebuffer width/height otherwhise repaint the whole screen
		if(x + w > vncclient->width || y + h > vncclient->height){
			Paint();
			return;
		}
		//printf("  SendingFrameBufferUpdateRequest(%d,%d,%d,%d)\n",x,y,w,h);
		//SendFramebufferUpdateRequest(vncclient,x,y,w,h,false);
		wxBitmap *bmp = VNCGenerateBitmap(x,y,w,h);
		if(bmp){
			dc.DrawBitmap(*bmp,x,y,false);
			delete bmp;
		}
		
		upd++;
	}
}

void VNCPanel::Paint(){
	if(!vncclient)
		return;
	VNCUpdateRect(0,0,vncclient->width,vncclient->height);
}

void VNCPanel::OnMouseEvent(wxMouseEvent& event){
	if(!vncclient)
		return;

	//wxPoint point = event.GetLogicalPosition(wxDC(this));
	wxPoint point = event.GetPosition();
	if(point.x < 0 || point.y < 0)
		return;
	// mouse just moving no buttons pressed most common case
	if(event.Moving()){
		DEBUG_MOUSE("moving: x:%d y:%d\n",point.x,point.y);
		SendPointerEvent(vncclient,point.x,point.y,0);
		return;
	}

	if(event.Entering()){
		HideCursor();
		return;
	}

	if(event.Leaving()){
		ShowCursor();
		return;
	}

	int button = 0;
	
	if(event.LeftIsDown())
		button |= rfbButton1Mask;
	if(event.MiddleIsDown())
		button |= rfbButton2Mask;
	if(event.RightIsDown())
		button |= rfbButton3Mask;
	
	DEBUG_MOUSE("event: x:%d y:%d button:%d\n",point.x,point.y,button);
	SendPointerEvent(vncclient,point.x,point.y,button);
}

void VNCPanel::HideCursor(){
	wxImage image(1,1);
	image.SetMask(true);
	image.SetMaskColour(0,0,0);
	wxCursor cursor(image);
	SetCursor(cursor);
}

void VNCPanel::ShowCursor(){
	SetCursor(wxNullCursor);
}

void VNCPanel::OnKeyDown(wxKeyEvent& event){
	if(vncclient)
		SendKeyEvent(vncclient,TranslateKeyCode(event),true);
}

void VNCPanel::OnKeyUp(wxKeyEvent& event){
	if(vncclient)
		SendKeyEvent(vncclient,TranslateKeyCode(event),false);
}

rfbKeySym VNCPanel::TranslateKeyCode(wxKeyEvent& event){
#if 0
	rfbKeySym k = 0;
	switch(event.GetKeyCode()){
	case WXK_BACK: k = XK_BackSpace; break;
	case WXK_TAB: k = XK_ISO_Left_Tab; break;
	case WXK_CLEAR: k = XK_Clear; break;
	case WXK_RETURN: k = XK_Return; break;
	case WXK_PAUSE: k = XK_Pause; break;
	case WXK_ESCAPE: k = XK_Escape; break;
	case WXK_SPACE: k = XK_space; break;
	case WXK_DELETE: k = XK_Delete; break;
	case WXK_NUMPAD0: k = XK_KP_0; break;
	case WXK_NUMPAD1: k = XK_KP_1; break;
	case WXK_NUMPAD2: k = XK_KP_2; break;
	case WXK_NUMPAD3: k = XK_KP_3; break;
	case WXK_NUMPAD4: k = XK_KP_4; break;
	case WXK_NUMPAD5: k = XK_KP_5; break;
	case WXK_NUMPAD6: k = XK_KP_6; break;
	case WXK_NUMPAD7: k = XK_KP_7; break;
	case WXK_NUMPAD8: k = XK_KP_8; break;
	case WXK_NUMPAD9: k = XK_KP_9; break;
	case WXK_NUMPAD_DECIMAL: k = XK_KP_Decimal; break;
	case WXK_NUMPAD_DIVIDE: k = XK_KP_Divide; break;
	case WXK_NUMPAD_MULTIPLY: k = XK_KP_Multiply; break;
	case WXK_NUMPAD_SUBTRACT: k = XK_KP_Subtract; break;
	case WXK_NUMPAD_ADD: k = XK_KP_Add; break;
	case WXK_NUMPAD_ENTER: k = XK_KP_Enter; break;
	case WXK_NUMPAD_EQUAL: k = XK_KP_Equal; break;
	case WXK_UP: k = XK_Up; break;
	case WXK_DOWN: k = XK_Down; break;
	case WXK_RIGHT: k = XK_Right; break;
	case WXK_LEFT: k = XK_Left; break;
	case WXK_INSERT: k = XK_Insert; break;
	case WXK_HOME: k = XK_Home; break;
	case WXK_END: k = XK_End; break;
	case WXK_PAGEUP: k = XK_Page_Up; break;
	case WXK_PAGEDOWN: k = XK_Page_Down; break;
	case WXK_F1: k = XK_F1; break;
	case WXK_F2: k = XK_F2; break;
	case WXK_F3: k = XK_F3; break;
	case WXK_F4: k = XK_F4; break;
	case WXK_F5: k = XK_F5; break;
	case WXK_F6: k = XK_F6; break;
	case WXK_F7: k = XK_F7; break;
	case WXK_F8: k = XK_F8; break;
	case WXK_F9: k = XK_F9; break;
	case WXK_F10: k = XK_F10; break;
	case WXK_F11: k = XK_F11; break;
	case WXK_F12: k = XK_F12; break;
	case WXK_F13: k = XK_F13; break;
	case WXK_F14: k = XK_F14; break;
	case WXK_F15: k = XK_F15; break;
	case WXK_NUMLOCK: k = XK_Num_Lock; break;
	case WXK_CAPITAL: k = XK_Caps_Lock; break;
	case WXK_SCROLL: k = XK_Scroll_Lock; break;
	case WXK_SHIFT: k = XK_Shift_L; break;
//	case SDLK_RSHIFT: k = XK_Shift_R; break;
//	case SDLK_LSHIFT: k = XK_Shift_L; break;
	case WXK_CONTROL: k = XK_Control_L; break;
//	case SDLK_RCTRL: k = XK_Control_R; break;
//	case SDLK_LCTRL: k = XK_Control_L; break;
	case WXK_ALT: k = XK_Alt_L; break;
//	case SDLK_RALT: k = XK_Alt_R; break;
//	case SDLK_LALT: k = XK_Alt_L; break;
//	case SDLK_RMETA: k = XK_Meta_R; break;
//	case SDLK_LMETA: k = XK_Meta_L; break;
	case WXK_WINDOWS_LEFT: k = XK_Super_L; break;	/* left windows key */
	case WXK_WINDOWS_RIGHT: k = XK_Super_R; break;	/* right windows key */
//	case SDLK_COMPOSE: k = XK_Compose; break;
//	case SDLK_MODE: k = XK_Mode_switch; break;
	case WXK_HELP: k = XK_Help; break;
	case WXK_PRINT: k = XK_Print; break;
//	case SDLK_SYSREQ: k = XK_Sys_Req; break;
//	case WXK_PAUSE: k = XK_Break; break;
	default: 
		k = event.GetKeyCode();
		break;
	}

	return k;
#endif	
	rfbKeySym keycode = event.GetRawKeyCode();
	DEBUG_KEYBOARD("keycode: %d",keycode);
	return keycode;
}

inline void VNCPanel::OnEraseBackground(wxEraseEvent &){};

extern "C" void VNCUpdateCallback(rfbClient* cl,int x,int y,int w,int h) {
	VNCPanel *panel = (VNCPanel*)rfbClientGetClientData(cl,(void *)dummy);
	if(panel){
		DEBUG_VNC("update callback: x:%d y:%d w:%d h:%d\n",x,y,w,h);
		panel->VNCUpdateRect(x,y,w,h);
	}
}

extern "C" rfbBool VNCResizeCallback(rfbClient *client){
	DEBUG_VNC("resize callback: w:%d h:%d\n",client->width,client->height);
	VNCPanel *panel = (VNCPanel*)rfbClientGetClientData(client,(void *)dummy);
	if(panel)
		return panel->VNCResize();
	return false;
}
