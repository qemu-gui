#ifndef QEMU_GUI_NOTEBOOK_H
#define QEMU_GUI_NOTEBOOK_H

#include <wx/window.h>
#include <wx/notebook.h>
#include <wx/panel.h>
#include <wx/html/htmlwin.h>
#include "homepage.h"

//DECLARE_EVENT_TYPE(EVT_NOTEBOOK_CONTEXT_CLOSE,wxID_ANY)

class QemuVM;
class VNCPanel;
class ConsolePanel;
class wxBoxSizer;
class QemuUINotebookPage;

class QemuUINotebook : public wxNotebook
{
public:
	QemuUINotebook(wxWindow *parent);
	// returns the page position of the homepage
	int ShowHomepage();
	void CloseHomepage();
	void CreateHomepage();
	bool IsHomepage(const int pos);
	bool HasHomepage();
	void AddVM(QemuVM *);
	QemuUINotebookPage* GetCurrentVMTab();
	QemuVM* GetCurrentVM();
private:
	void ShowContextMenu(const wxPoint& pos);
	void OnMouseEvent(wxMouseEvent& event);
	wxNotebookPage *homepage;
	int contextTabID;
	DECLARE_EVENT_TABLE()
};

class QemuUINotebookPage : public wxPanel 
{
public:
	QemuUINotebookPage(wxWindow *parent,QemuVM *vm = NULL,wxPanel *info = NULL, VNCPanel *vncpanel =NULL,ConsolePanel *console = NULL);
	void SetVM(QemuVM *vm);
	QemuVM* GetVM();
	VNCPanel* GetScreen();
	void SetVNCPanel(VNCPanel *vncpanel);
	ConsolePanel* GetConsole();
	void SetConsole(ConsolePanel *console);
	void ShowInfo();
	void ShowScreen();
	void ShowConsole();
	bool IsInfoPageActive();
	bool IsScreenActive();
	bool IsConsoleActive();
protected:
	QemuVM *vm;
	wxPanel *info;
	VNCPanel *vncpanel;
	ConsolePanel *console;
	void *active;
	wxBoxSizer *sizer;
};

enum {
	QEMU_VM_NOTEBOOK = wxID_HIGHEST + 300,
	QEMU_VM_NOTEBOOK_CONTEXT_CLOSE
};

#endif
