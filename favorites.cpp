#include <wx/wx.h>
#include <wx/sizer.h>
#include <wx/listctrl.h>
#include <wx/menu.h>
#include "mainframe.h"
#include "qemu-ui.h"
#include "favorites.h"
#include "vmdialog.h"
#include "vm.h"

BEGIN_EVENT_TABLE(QemuUIFavoritesWindow,wxPanel)
	EVT_LIST_ITEM_RIGHT_CLICK(wxID_ANY,QemuUIFavoritesWindow::OnContextMenu)
	EVT_LIST_ITEM_ACTIVATED(wxID_ANY,QemuUIFavoritesWindow::OnListItemActivated)
	EVT_MENU(ID_FAVORITES_CONTEXT_OPEN,QemuUIFavoritesWindow::OnContextMenuOpen)
	EVT_MENU(ID_FAVORITES_CONTEXT_EDIT,QemuUIFavoritesWindow::OnContextMenuEdit)
	EVT_MENU(ID_FAVORITES_CONTEXT_REMOVE,QemuUIFavoritesWindow::OnContextMenuRemove)
END_EVENT_TABLE()

QemuUIFavoritesWindow::QemuUIFavoritesWindow(wxWindow* parent) 
                      :wxPanel(parent,wxID_ANY)
{

	wxBoxSizer *titlesizer = new wxBoxSizer(wxHORIZONTAL);
	label = new wxStaticText(this,-1,_("Favorites"));
	button = new wxButton(this,ID_FAVORITES_CLOSE,_(" "),wxDefaultPosition,wxSize(20,20));
	list = new wxListCtrl(this); 
	list->SetSingleStyle(wxLC_SMALL_ICON);
	list->SetSingleStyle(wxLC_NO_HEADER);
	list->SetSingleStyle(wxLC_REPORT);
	list->SetSingleStyle(wxLC_LIST);
	list->SetSingleStyle(wxLC_SINGLE_SEL);

	titlesizer->Add(
		label,
		1,
		wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL,
		3
	);
	
	titlesizer->Add(
		button,
		0,
		wxALL|wxALIGN_CENTER_VERTICAL,
		3
	);

	sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(titlesizer,0,wxEXPAND);
	sizer->Add(list,1,wxEXPAND);

	LoadFavorites();
	SetSizer(sizer);
	Layout();
}

QemuUIFavoritesWindow::~QemuUIFavoritesWindow(){
	wxConfigBase *favorites = wxConfigBase::Get();
	favorites->DeleteGroup(wxT("/favorites"));
	favorites->SetPath(wxT("/favorites"));
	wxString *config;
	for(int i = 0; i < list->GetItemCount(); i++){
		config = (wxString *)list->GetItemData(i);
		wxString key;
		key << i;
		favorites->Write(key,*config);
		delete config;
	}
}


void QemuUIFavoritesWindow::LoadFavorites(){
	wxConfigBase *favorites = wxConfigBase::Get();
	if(favorites->HasGroup(wxT("/favorites"))){
		favorites->SetPath(wxT("/favorites"));
		wxString key = wxT("0"); 
		int index = 0;
		wxString config;
		while(favorites->HasEntry(key)){
			config = favorites->Read(key);
			QemuVM *vm = new QemuVM(config);
			AddVM(vm);
			delete vm;
			key.Printf(wxT("%d"),++index);
		}
	}

}

void QemuUIFavoritesWindow::AddVM(QemuVM *vm){
	wxString config = vm->GetConfigFile();
	for(int i = 0; i < list->GetItemCount(); i++){
		wxString *c = (wxString *)list->GetItemData(i);
		if(config.Cmp(*c) == 0)
			return;
	}

	wxListItem item;
	item.SetText(vm->GetTitle());
	item.SetData(new wxString(config));
	list->InsertItem(item);
}

void QemuUIFavoritesWindow::OnContextMenu(wxListEvent& event){
	if(event.GetIndex() == -1)
		return;
	wxMenu menu;
	menu.Append(ID_FAVORITES_CONTEXT_OPEN,_("&Open"));
	menu.Append(ID_FAVORITES_CONTEXT_EDIT,_("&Edit"));
	menu.AppendSeparator();
	menu.Append(ID_FAVORITES_CONTEXT_REMOVE,_("&Remove"));
	PopupMenu(&menu);
}

void QemuUIFavoritesWindow::OnContextMenuOpen(wxCommandEvent& WXUNUSED(event)){
	OpenVM();
}

void QemuUIFavoritesWindow::OnListItemActivated(wxListEvent& WXUNUSED(event)){
	OpenVM();
}

void QemuUIFavoritesWindow::OpenVM(){
	long index=list->GetNextItem(-1,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);
	if(index == -1)
		return;
	
	wxString *config = (wxString*)list->GetItemData(index);
	QemuGUI::GetMainFrame()->OpenVM(*config);
}

void QemuUIFavoritesWindow::OnContextMenuEdit(wxCommandEvent& WXUNUSED(event)){
	long index=list->GetNextItem(-1,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);
	if(index == -1)
		return;
	
	wxString *config = (wxString*)list->GetItemData(index);
	QemuVM *vm = new QemuVM(*config); 
	QemuVMConfigDialog *dialog = new QemuVMConfigDialog(this,vm);
	if(dialog->ShowModal() == wxID_OK){
		dialog->Save();
		list->SetItemText(index,vm->GetTitle());
	}
	dialog->Destroy();
	delete vm;
}

void QemuUIFavoritesWindow::OnContextMenuRemove(wxCommandEvent& WXUNUSED(event)){
	long index=list->GetNextItem(-1,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);
	if(index == -1)
		return;
	
	delete (wxString*)list->GetItemData(index);
	list->DeleteItem(index);
}
