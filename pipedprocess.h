#ifndef QEMU_GUI_PIPED_PROCESS_H
#define QEMU_GUI_PIPED_PROCESS_H

#include <wx/event.h>
#include <wx/process.h>
#include <wx/timer.h>
#include "events.h"
//#include <wx/wx.h>
//#include <wx/txtstrm.h>

DECLARE_EVENT_TYPE(EVT_PROCESS_INPUT,wxID_ANY)
DECLARE_EVENT_TYPE(EVT_PROCESS_OUTPUT,wxID_ANY)
DECLARE_EVENT_TYPE(EVT_PROCESS_CREATED,wxID_ANY)
DECLARE_EVENT_TYPE(EVT_PROCESS_TERMINATED,wxID_ANY)
//WX_DEFINE_ARRAY_PTR(wxEvtHandler *,EvtHandlerArray);

class PipedProcess : public wxProcess
{
public:
	PipedProcess();
	~PipedProcess();
	
	/**
	 * runs the passed command, and checks for input in the given
	 * interval. returns the pid or 0 if an error occured.
	 */
	long Launch(const wxString& cmd,unsigned int interval = 100);

	/**
	 * sends the given string to the childs stdin
	 */
	void SendString(const wxString& text);

	/**
	 * checks whether the child process printed something to it's
	 * stdout and if so generates EVT_PROCESS_INPUT events
	 */
	bool HasInput();

	/**
	 * adds an event handler which will be notified if an event occurs
	 */
	void AddEventHandler(wxEvtHandler *handler);
private:
	/**
	 * gets called if the child process terminates, generates an
	 * EVT_PROCESS_TERMINATED event.
	 */
	virtual void OnTerminate(int pid, int status);

	/**
	 * gets called according to the interval passed to Launch()
	 * and checks if there is some input available by calling
	 * HasInput();
	 */
	void OnTimer(wxTimerEvent& event);
	void OnIdle(wxIdleEvent& event);
	
	void SendEvent(wxCommandEvent& event);
	
	bool running;
	wxTimer timer;
	EvtHandlerArray evtHandlers;
	long pid;
	DECLARE_EVENT_TABLE();
};

enum {
	ID_QEMU_TIMER_POLL_PROCESS = 10002
};

#endif
