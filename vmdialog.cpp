#include <wx/wx.h>
#include <wx/spinctrl.h>
#include "vm.h"
#include "vmdialog.h"
#include "guibuilder.h"

enum { ID_CHOOSE_DEVICE_BUTTON };

class QemuDeviceWidget : public wxPanel {
public:
	QemuDeviceWidget(wxWindow *parent,wxString label) : wxPanel(parent){
		wxGuiBuilder gb(wxT(
			"     H{'")+label+wxT("'|[------]|,1:selection ['Browse']:button}+ "
		));

		gb << wxGbTextCtrl(wxT("selection"), &selection, wxID_ANY);
		gb << wxGbButton(wxT("button"),NULL,ID_CHOOSE_DEVICE_BUTTON);
		if(gb.Build(this, wxID_ANY)){
			this->SetSizerAndFit(gb.GetSizer());
		}
	}

	wxString GetValue(){
		return selection->GetValue();
	}

	void SetValue(const wxString& value){
		selection->SetValue(value);
	}

protected:
	virtual void DoSelection(){
		wxString dirHome;
		wxGetHomeDir(&dirHome);

		wxFileDialog *dialog = new wxFileDialog(
			this,
			_("Choose a file"),
			dirHome
		);

		if(dialog->ShowModal() == wxID_OK){
			selection->SetValue(dialog->GetPath());
		}

		dialog->Destroy();
	}
	
	wxTextCtrl *selection;
private:

	void OnChooseDevice(wxCommandEvent& WXUNUSED(event)){
		DoSelection();
	}

	DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(QemuDeviceWidget,wxPanel)
	EVT_BUTTON(ID_CHOOSE_DEVICE_BUTTON,QemuDeviceWidget::OnChooseDevice)
END_EVENT_TABLE()

class QemuDirectoryWidget : public QemuDeviceWidget {
public:
	QemuDirectoryWidget(wxWindow *parent, wxString label) : 
		QemuDeviceWidget(parent,label){};
private:
	void DoSelection(){
		wxString dirHome;
		wxGetHomeDir(&dirHome);

		wxDirDialog *dialog = new wxDirDialog(
			this,
			_("Choose a directory"),
			dirHome,
			wxDD_DEFAULT_STYLE | wxDD_NEW_DIR_BUTTON
		);

		if(dialog->ShowModal() == wxID_OK){
			selection->SetValue(dialog->GetPath());
		}

		dialog->Destroy();
	}
};

QemuVMConfigDialog::QemuVMConfigDialog(wxWindow *parent,QemuVM *vm)
                   :wxPropertySheetDialog(parent, wxID_ANY, wxEmptyString)
{
	this->vm = vm;
	SetTitle(_("Edit virtual machine settings"));
	CreateButtons(wxOK|wxCANCEL|wxHELP);

	wxBookCtrlBase *notebook = GetBookCtrl();
	notebook->AddPage(CreateGeneralPage(notebook),_("General"));
	notebook->AddPage(CreateDevicePage(notebook),_("Devices"));
	notebook->AddPage(CreateNetworkPage(notebook),_("Network"));
	notebook->AddPage(CreateMiscPage(notebook),_("Misc"));
	Load();	
	LayoutDialog();
}

QemuVMConfigDialog::~QemuVMConfigDialog(){
}

const wxString QemuVMConfigDialog::keyboardLayouts[] = {
	wxT("ar"), wxT("de-ch"), wxT("es"), wxT("fo"), wxT("fr-ca"), wxT("hu"),
      	wxT("ja"), wxT("mk"), wxT("no"), wxT("pt-br"), wxT("sv"), wxT("da"),
	wxT("en-gb"), wxT("et"), wxT("fr"), wxT("fr-ch"), wxT("is"), wxT("lt"),
	wxT("nl"), wxT("pl"), wxT("ru"), wxT("th"), wxT("de"), wxT("en-us"),
	wxT("fi"), wxT("fr-be"), wxT("hr"),wxT("it"), wxT("lv"), wxT("nl-be"),
	wxT("pt"), wxT("sl"), wxT("tr")
};

void QemuVMConfigDialog::Load(){
	title->SetValue(vm->GetTitle());
	dir->SetValue(vm->GetPath());
	smp->SetRange(1,255);
	smp->SetValue(vm->GetSmp());
	memory->SetRange(0,8*1024);
	memory->SetValue(vm->GetMemory());
	hda->SetValue(vm->GetHd(0));
	hdb->SetValue(vm->GetHd(1));
	hdc->SetValue(vm->GetHd(2));
	hdd->SetValue(vm->GetHd(3));
	cdrom->SetValue(vm->GetCdRom());
	fda->SetValue(vm->GetFd(0));
	fdb->SetValue(vm->GetFd(1));
	smb->SetValue(vm->GetSMB());
	tftp->SetValue(vm->GetTFTP());
	wxString boot = vm->GetBoot();
	boota->SetValue(boot.IsSameAs(wxT("a")));
	bootc->SetValue(boot.IsSameAs(wxT("c")) || boot.IsEmpty());
	bootd->SetValue(boot.IsSameAs(wxT("d")));
	keyboard->SetStringSelection(vm->GetKeyboardLayout());
	localtime->SetValue(vm->GetLocalTime());
	win2khack->SetValue(vm->GetWin2kHack());
	stdvga->SetValue(vm->GetStdVga());
	noacpi->SetValue(vm->GetNoACPI());
	snapshotmode->SetValue(vm->GetSnapshotMode());
	vncdisplay->SetValue(vm->GetVNCDisplay());
	vncdisplay->SetRange(-1,255);
	cmdlineargs->SetValue(vm->GetCmdlineArguments());
}

void QemuVMConfigDialog::Save(){
	vm->SetTitle(title->GetValue());
	vm->SetSmp(smp->GetValue());
	vm->SetMemory(memory->GetValue());
	vm->SetHd(0,hda->GetValue());
	vm->SetHd(1,hdb->GetValue());
	vm->SetHd(2,hdc->GetValue());
	vm->SetHd(3,hdd->GetValue());
	vm->SetCdRom(cdrom->GetValue());
	vm->SetFd(0,fda->GetValue());
	vm->SetFd(1,fdb->GetValue());
	vm->SetSMB(smb->GetValue());
	vm->SetTFTP(tftp->GetValue());

	wxString boot;
	if(boota->GetValue())
		boot = wxT("a");
	else if(bootc->GetValue())
		boot = wxT("c");
	else
		boot = wxT("d");
	vm->SetBoot(boot);

	vm->SetKeyboardLayout(keyboard->GetStringSelection());
	vm->SetLocalTime(localtime->GetValue());
	vm->SetWin2kHack(win2khack->GetValue());
	vm->SetStdVga(stdvga->GetValue());
	vm->SetNoACPI(noacpi->GetValue());
	vm->SetSnapshotMode(snapshotmode->GetValue());
	vm->SetVNCDisplay(vncdisplay->GetValue());
	vm->SetCmdlineArguments(cmdlineargs->GetValue());
	vm->SaveConfig();
}

wxPanel* QemuVMConfigDialog::CreateGeneralPage(wxWindow *parent){
	wxPanel *panel = new wxPanel(parent);
	wxGuiBuilder gb(wxString::FromAscii(
		"    V{                                         "
		"         V'Set your virtual machine title'{    "
		"              [----------------]+:title        "
		"         }+                                    "
		"                                               "
		"         V'Select the operating system'{       "
		"            ( ) 'Microsoft Windows'<           "
		"            ( ) 'GNU/Linux'<                   "
		"            ( ) '*BSD'<                        "
		"            (*) 'Another'<                     "
		"         }+                                    "
		"                                               "
		"         V'Set a directory'{                   "
//		"              [----------------]+:directory    "
		"             $1.0a+                            "
		"         }+                                    "
		"    }                                          "
	));
	gb << wxGbTextCtrl(wxT("title"), &title, wxID_ANY);
	gb << wxGbWindowStub(1,dir = new QemuDirectoryWidget(panel,wxT("")));
	if(gb.Build(panel, wxID_ANY)){
	        panel->SetSizerAndFit(gb.GetSizer()); 
	}
	return panel;
}

wxPanel* QemuVMConfigDialog::CreateDevicePage(wxWindow *parent){
	wxPanel *panel = new wxPanel(parent);
	wxGuiBuilder gb(wxString::FromAscii(
		"    V{                                            "
		"         %w<                                      "
		"         H'CPU / RAM'{                            "
		"             'SMP:'|$1+                           "
		"             'RAM:'|$2+                           "
		"         }+                                       "
		
		"         V'Harddisks'{                            "
		"             $3.0a+                               "
		"             $4.0a+                               "
		"             $5.0a+                               "
		"             $6.0a+                               "
	/*
		"            H{'hda:'|[------]|,1:hda ['Browse']:button1}.0bt+ "
		"            H{'hdb:'|[------]|,1:hdb ['Browse']:button2}.0bt+ "
		"            H{'hdc:'|[------]|,1:hdc ['Browse']:button3}.0bt+ "
		"            H{'hdd:'|[------]|,1:hdd ['Browse']:button4}.0bt+ "
	*/
		"	  }+                                       "

		"         V'CD-ROM'{                               "
		"             $7.0a+                               "
		"	  }+                                       "

		"         V'Floppy disks'{                         "
		"             $8.0a+                               "
		"             $9.0a+                               "
		"	  }+                                       "
		"    }                                             "
	));

	gb << wxGbWindowStub(1,smp = new wxSpinCtrl(panel));
	gb << wxGbWindowStub(2,memory = new wxSpinCtrl(panel));
	gb << wxGbWindowStub(3,hda = new QemuDeviceWidget(panel,wxT("hda:")));
	gb << wxGbWindowStub(4,hdb = new QemuDeviceWidget(panel,wxT("hdb:")));
	gb << wxGbWindowStub(5,hdc = new QemuDeviceWidget(panel,wxT("hdc:")));
	gb << wxGbWindowStub(6,hdd = new QemuDeviceWidget(panel,wxT("hdd:")));
	gb << wxGbWindowStub(7,cdrom=new QemuDeviceWidget(panel,wxT("cdrom:")));
	gb << wxGbWindowStub(8,fda = new QemuDeviceWidget(panel,wxT("fda:")));
	gb << wxGbWindowStub(9,fdb = new QemuDeviceWidget(panel,wxT("fdb:")));

	if(gb.Build(panel, wxID_ANY)){
	        panel->SetSizerAndFit(gb.GetSizer()); 
	}

	return panel;
}

wxPanel* QemuVMConfigDialog::CreateNetworkPage(wxWindow *parent){
	wxPanel *panel = new wxPanel(parent);
	wxGuiBuilder gb(wxString::FromAscii(
		"    V{                                            "
		"         V'SMB'{                                  "
		"             $1.0a+                               "
		"	  }+                                       "

		"         V'TFTP'{                                  "
		"             $2.0a+                               "
		"	  }+                                       "
		"    }                                             "
	));
	
	gb << wxGbWindowStub(1,smb = new QemuDirectoryWidget(panel,wxT("")));
	gb << wxGbWindowStub(2,tftp= new QemuDirectoryWidget(panel,wxT("")));
	
	if(gb.Build(panel, wxID_ANY)){
	        panel->SetSizerAndFit(gb.GetSizer()); 
	}

	return panel;
}

wxPanel* QemuVMConfigDialog::CreateMiscPage(wxWindow *parent){
	wxPanel *panel = new wxPanel(parent);
	wxGuiBuilder gb(wxString::FromAscii(
		"    V{                                                 "
		"         %w<                                           "
		"         H'Select the boot media'{                     "
		"            ( ) 'Floppy disk':boota                    "
		"            (*) 'Hard disk':bootc                      "
		"            ( ) 'CD-ROM':bootd                         "
		"         }<+                                           "

		"         H'Keyboard layout'{                           "
		"             $1+                                       "
		"         }+                                            "
		
		"         V'Various options'{                           "
		"            [ ] 'Set the real time clock to local time [default=utc]':localtime "
		"            [ ] 'Enable win2k-hack, use it when installing Windows 2000\nto avoid a disk full bug':win2khack"
		"            [ ] 'Simulate a standard VGA card with Bochs VBE extensions\n(default is Cirrus Logic GD5446 PCI VGA). If your guest OS\nsupports the VESA 2.0 VBE extensions (e.g. Windows XP) and\nif you want to use high resolution modes (>= 1280x1024x16)\nthen you should use this option.':stdvga "
		"            [ ] 'Disable ACPI (Advanced Configuration and Power Interface)\nsupport. Use it if your guest OS complains about ACPI\nproblems (PC target machine only).':noacpi "
		"            [ ] 'Snapshot mode, write to temporary files instead of disk image files.':snapshotmode "
		"         }+                                            "

		"         H'VNC Display'{                               "
		"             'VNC display number:'|$2+                 "
		"         }+                                            "
	
		"         H'Additional command line arguments'{         "
		"              [-------------]+,1:cmdlineargs           "
		"         }+                                            "
		"    }                                                  "
	));

	gb << wxGbRadioButton(wxT("boota"), &boota, wxID_ANY);
	gb << wxGbRadioButton(wxT("bootc"), &bootc, wxID_ANY);
	gb << wxGbRadioButton(wxT("bootd"), &bootd, wxID_ANY);
	wxArrayString layouts(
		sizeof(keyboardLayouts) / sizeof(keyboardLayouts[0]),
		keyboardLayouts
	);
	layouts.Sort();
	gb << wxGbWindowStub(1,keyboard = new wxChoice(
		panel,
		wxID_ANY,
		wxDefaultPosition,
		wxDefaultSize,
		layouts
	));
	gb << wxGbCheckBox(wxT("localtime"), &localtime, wxID_ANY);
	gb << wxGbCheckBox(wxT("win2khack"), &win2khack, wxID_ANY);
	gb << wxGbCheckBox(wxT("stdvga"), &stdvga, wxID_ANY);
	gb << wxGbCheckBox(wxT("noacpi"), &noacpi, wxID_ANY);
	gb << wxGbCheckBox(wxT("snapshotmode"), &snapshotmode, wxID_ANY);
	gb << wxGbWindowStub(2,vncdisplay = new wxSpinCtrl(panel));
	gb << wxGbTextCtrl(wxT("cmdlineargs"), &cmdlineargs, wxID_ANY);
	if(gb.Build(panel, wxID_ANY)){
	        panel->SetSizerAndFit(gb.GetSizer()); 
	}

	return panel;
}
