#ifndef QEMU_GUI_HOMEPAGE
#define QEMU_GUI_HOMEPAGE

DECLARE_EVENT_TYPE(EVT_NEW_VM,wxID_ANY)
DECLARE_EVENT_TYPE(EVT_OPEN_VM,wxID_ANY)

class WelcomeHtmlWindow : public wxHtmlWindow
{
public:
	WelcomeHtmlWindow(wxWindow *parent);
private:
	virtual void OnLinkClicked(const wxHtmlLinkInfo& link);
};

#endif
