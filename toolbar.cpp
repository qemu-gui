#include <wx/intl.h>
#include "toolbar.h"
#include "events.h"

//#if USE_XPM_BITMAPS
    #include "icons/new.xpm"
    #include "icons/open.xpm"
    #include "icons/save.xpm"
    #include "icons/copy.xpm"
    #include "icons/cut.xpm"
    #include "icons/preview.xpm"
    #include "icons/print.xpm"
    #include "icons/help.xpm"
//#endif // USE_XPM_BITMAPS

QemuUIToolBar::QemuUIToolBar(wxWindow *parent,long style)
              :wxToolBar(parent,wxID_ANY,wxDefaultPosition,wxDefaultSize,style){

	SetWindowStyle(style);

	AddTool(
		QEMU_VM_START,
		_("Power on"),
		wxBitmap(QEMU_XPM_SHUTDOWN),
		_("Power on this VM")
	);
    
	AddTool(
		QEMU_VM_PAUSE,
		_("Pause"),
		wxBitmap(QEMU_XPM_PAUSE),
		_("Pause this VM")
	);
	
	AddTool(
		QEMU_VM_SHUTDOWN,
		_("Power off"),
		wxBitmap(QEMU_XPM_START),
		_("Power off this VM")
	);
    
	AddTool(
		QEMU_VM_RESET,
		_("Reset"),
		wxBitmap(QEMU_XPM_RESET),
		_("Reset this VM")
	);
    
	AddTool(
		QEMU_VM_KILL,
		_("Kill VM"),
		wxBitmap(QEMU_XPM_RESET),
		_("Kill this VM")
	);

	AddSeparator();
	
	AddTool(
		QEMU_VM_SNAPSHOT,
		_("Snapshot"),
		wxBitmap(QEMU_XPM_RESET),
		_("Take a snapshot of the current vm state")
	);

	AddTool(
		QEMU_VM_REVERT_SNAPSHOT,
		_("Revert"),
		wxBitmap(QEMU_XPM_RESET),
		_("Revert to last snapshot")
	);

	AddSeparator();
	    
	AddTool(
		QEMU_TOOLBAR_DISPLAY_INFO,
		_("Info"),
		wxBitmap(QEMU_XPM_RESET),
		_("Show information about this vm"),
		wxITEM_CHECK
	);
	
	AddTool(
		QEMU_TOOLBAR_DISPLAY_SCREEN,
		_("Screen"),
		wxBitmap(QEMU_XPM_RESET),
		_("Show the screen of this vm."),
		wxITEM_CHECK
	);
	
	AddTool(
		QEMU_TOOLBAR_DISPLAY_CONSOLE,
		_("Console"),
		wxBitmap(QEMU_XPM_RESET),
		_("Show the console of this vm."),
		wxITEM_CHECK
	);

	AddSeparator();
	    
	AddTool(
		QEMU_TOOLBAR_SEND_VM_COMMAND,
		_("VM command"),
		wxBitmap(QEMU_XPM_RESET),
		_("Send a command to the qemu monitor.")
	);
	
	SetToolBitmapSize(wxSize(24,24));
	Realize();
} 

QemuUIToolBar::~QemuUIToolBar(){};

void QemuUIToolBar::Disable(){
	PowerOn(false);
	PowerOff(false);
	Pause(false);
	Reset(false);
	Kill(false);
	Snapshot(false);
	RevertSnapshot(false);
	DisableDisplayArea();
	SendVMCommand(false);
}

void QemuUIToolBar::EnableDisplayArea(){
	EnableTool(QEMU_TOOLBAR_DISPLAY_INFO,true);
	EnableTool(QEMU_TOOLBAR_DISPLAY_SCREEN,true);
	EnableTool(QEMU_TOOLBAR_DISPLAY_CONSOLE,true);
}

void QemuUIToolBar::DisableDisplayArea(){
	ToggleTool(QEMU_TOOLBAR_DISPLAY_INFO,false);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_SCREEN,false);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_CONSOLE,false);
	EnableTool(QEMU_TOOLBAR_DISPLAY_INFO,false);
	EnableTool(QEMU_TOOLBAR_DISPLAY_SCREEN,false);
	EnableTool(QEMU_TOOLBAR_DISPLAY_CONSOLE,false);
}

void QemuUIToolBar::PowerOn(bool state){
	EnableTool(QEMU_VM_START,state);
}

void QemuUIToolBar::PowerOff(bool state){
	EnableTool(QEMU_VM_SHUTDOWN,state);
}

void QemuUIToolBar::Pause(bool state){
	EnableTool(QEMU_VM_PAUSE,state);
}

void QemuUIToolBar::Reset(bool state){
	EnableTool(QEMU_VM_RESET,state);
}

void QemuUIToolBar::Kill(bool state){
	EnableTool(QEMU_VM_KILL,state);
}

void QemuUIToolBar::Snapshot(bool state){
	EnableTool(QEMU_VM_SNAPSHOT,state);
}

void QemuUIToolBar::RevertSnapshot(bool state){
	EnableTool(QEMU_VM_REVERT_SNAPSHOT,state);
}

void QemuUIToolBar::ShowInfo(){
	ToggleTool(QEMU_TOOLBAR_DISPLAY_INFO,true);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_SCREEN,false);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_CONSOLE,false);
}

void QemuUIToolBar::ShowScreen(){
	ToggleTool(QEMU_TOOLBAR_DISPLAY_INFO,false);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_SCREEN,true);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_CONSOLE,false);
}

void QemuUIToolBar::ShowConsole(){
	ToggleTool(QEMU_TOOLBAR_DISPLAY_INFO,false);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_SCREEN,false);
	ToggleTool(QEMU_TOOLBAR_DISPLAY_CONSOLE,true);
}

void QemuUIToolBar::SendVMCommand(bool state){
	EnableTool(QEMU_TOOLBAR_SEND_VM_COMMAND,state);
}

void QemuUIToolBar::EnableDisplayScreen(bool state){
	EnableTool(QEMU_TOOLBAR_DISPLAY_SCREEN,state);
}
