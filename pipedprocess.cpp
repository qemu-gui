#include <wx/timer.h>
#include <wx/txtstrm.h>
#include "pipedprocess.h"

DEFINE_EVENT_TYPE(EVT_PROCESS_INPUT)
DEFINE_EVENT_TYPE(EVT_PROCESS_OUTPUT)
DEFINE_EVENT_TYPE(EVT_PROCESS_CREATED)
DEFINE_EVENT_TYPE(EVT_PROCESS_TERMINATED)

BEGIN_EVENT_TABLE(PipedProcess, wxProcess)
    EVT_TIMER(ID_QEMU_TIMER_POLL_PROCESS, PipedProcess::OnTimer)
    EVT_IDLE(PipedProcess::OnIdle)
END_EVENT_TABLE()

PipedProcess::PipedProcess() : wxProcess(){
	Redirect();
}

PipedProcess::~PipedProcess()
{
}

long PipedProcess::Launch(const wxString& cmd,unsigned int interval){
	wxCommandEvent event(EVT_PROCESS_CREATED,wxID_ANY);
	event.SetEventObject(this);
	event.SetString(cmd + wxT("\n"));
	SendEvent(event);

   	pid = wxExecute(cmd, wxEXEC_ASYNC | wxEXEC_NODISABLE, this);
	running = true;
	if (pid)
	{
		/* close stdin of child process */
		CloseOutput();
		timer.SetOwner(this, ID_QEMU_TIMER_POLL_PROCESS);
		timer.Start(interval);
	}
	return pid;
}

void PipedProcess::SendString(const wxString& text)
{
#if 0
	if(!running)
		return;
	
	wxOutputStream *out = GetOutputStream();
	if(out)
	{
		wxTextOutputStream sin(*out);
		wxString msg = text + wxT("\n");
		sin.WriteString(msg);
		wxCommandEvent event(EVT_PROCESS_OUTPUT,wxID_ANY);
		event.SetEventObject(this);
		event.SetString(msg);
		SendEvent(event);
	}
#endif
}

bool PipedProcess::HasInput()
{
	bool hasInput = false;

	if (IsInputAvailable())
	{
		wxInputStream *in = GetInputStream();
		if(in->CanRead()){
			char buffer[4096];
			in->Read(buffer,WXSIZEOF(buffer) - 1);
			buffer[in->LastRead()] = '\0';
			// remove the first line, because it contains some non printable characters
			char *p = strchr(buffer,'\n');
			if(p){
				wxString msg(++p,wxConvUTF8,in->LastRead() - (p - buffer));
				wxCommandEvent event(EVT_PROCESS_INPUT,wxID_ANY);
				event.SetEventObject(this);
				event.SetString(msg);
				SendEvent(event);
			}
		}
		hasInput = true;
	}

	if(IsErrorAvailable())
	{
		wxTextInputStream serr(*GetErrorStream());

		wxString msg;
		msg << serr.ReadLine();

		wxCommandEvent event(EVT_PROCESS_INPUT,wxID_ANY);
		event.SetEventObject(this);
		event.SetString(msg);
		SendEvent(event);

		hasInput = true;
	}

	return hasInput;
}

void PipedProcess::OnTerminate(int pid, int status)
{
	running = false;
	timer.Stop();
	// show the rest of the output
	while (HasInput())
        	;

	wxCommandEvent event(EVT_PROCESS_TERMINATED,wxID_ANY);
	event.SetEventObject(this);
	event.SetInt(status);
	SendEvent(event);

	//delete this;
}

void PipedProcess::OnTimer(wxTimerEvent& event){
	while (HasInput())
		;
}

void PipedProcess::OnIdle(wxIdleEvent& event){
	while (HasInput())
		;
}

void PipedProcess::AddEventHandler(wxEvtHandler *handler){
	evtHandlers.Add(handler);
}

void PipedProcess::SendEvent(wxCommandEvent& event){
	for(int i = 0; i < evtHandlers.GetCount(); i++){
		evtHandlers.Item(i)->ProcessEvent(event);
	}
}
