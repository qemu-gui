#include <wx/wx.h>
#include "statusbar.h"
#include "mainframe.h"
#include "vm.h"

#include "icons/save.xpm"
#include "icons/preview.xpm"

#define STATUSBAR_FIELDS 5
#define STATUSBAR_MIN_HEIGHT 10

BEGIN_EVENT_TABLE(QemuUIStatusBar,wxStatusBar)
	EVT_SIZE(QemuUIStatusBar::OnSize)
END_EVENT_TABLE()

QemuUIStatusBar::QemuUIStatusBar(QemuMainFrame *parent)
                :wxStatusBar(parent, wxID_ANY)
{
	static const int widths[STATUSBAR_FIELDS] = { -1, 20, 20, 20, 20 };
	static const int styles[STATUSBAR_FIELDS] = { 
		wxSB_FLAT,
		wxSB_FLAT,
		wxSB_FLAT,
		wxSB_FLAT,
		wxSB_FLAT 
	};

	SetFieldsCount(STATUSBAR_FIELDS);
	SetStatusWidths(STATUSBAR_FIELDS,widths);
	SetStatusStyles(STATUSBAR_FIELDS,styles);

//	SetMinHeight(STATUSBAR_MIN_HEIGHT);

	this->floppy = new wxStaticBitmap(this,wxID_ANY,wxBitmap(paste_xpm));
/*	floppy->Connect(
		wxID_ANY,
		wxEVT_CONTEXT_MENU,
		wxContextMenuEventHandler(QemuUIStatusBar::OnFloppyContextMenu),
		NULL,
		this
	);
*/	
	this->cdrom = new wxStaticBitmap(this,ID_STATUSBAR_CDROM,wxBitmap(save_xpm));
	cdrom->Connect(
		ID_STATUSBAR_CDROM,
		wxEVT_CONTEXT_MENU,
		wxContextMenuEventHandler(QemuUIStatusBar::OnCdRomContextMenu),
		NULL,
		this
	);

}

void QemuUIStatusBar::OnCdRomContextMenu(wxContextMenuEvent& event){
	if(!cdrom->IsEnabled())
		return;
	wxMenu menu;
	menu.AppendCheckItem(ID_STATUSBAR_CDROM_CONNECT,_("Connect"));
	QemuMainFrame *frame = (QemuMainFrame*) GetParent();
	if(frame->GetCurrentVM()->IsCdRomConnected()){
		menu.Check(ID_STATUSBAR_CDROM_CONNECT,true);
	}
	PopupMenu(&menu);
}

void QemuUIStatusBar::OnSize(wxSizeEvent& event){
	const int width = event.GetSize().GetWidth();
	int spacer = 0;
#ifdef __WXMSW__
	spacer = 10;
#endif
	floppy->Move(width - spacer - 40,5);
	cdrom->Move(width - spacer - 20,5);
	event.Skip();
}

void QemuUIStatusBar::Disable(){
	floppy->Disable();
	cdrom->Disable();
}

void QemuUIStatusBar::Enable(){
	floppy->Enable();
	cdrom->Enable();
}
