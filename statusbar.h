#ifndef QEMU_GUI_STATUS_BAR_H 
#define QEMU_GUI_STATUS_BAR_H 

#include <wx/statusbr.h>

class wxStatusBar;
class wxStaticBitmap;
class QemuMainFrame;

class QemuUIStatusBar : public wxStatusBar
{
public:
	QemuUIStatusBar(QemuMainFrame *parent);
	void Disable();
	void Enable();
private:
	void OnCdRomContextMenu(wxContextMenuEvent& event);
	void OnSize(wxSizeEvent& event);
	wxStaticBitmap *floppy;
	wxStaticBitmap *cdrom;
	wxStaticBitmap *hd;
	wxStaticBitmap *net;
	DECLARE_EVENT_TABLE()
};

enum {
	ID_STATUSBAR_CDROM,
	ID_STATUSBAR_CDROM_CONNECT 
};

#endif
