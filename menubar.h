#ifndef QEMU_GUI_MENUBAR_H
#define QEMU_GUI_MENUBAR_H

#include <wx/menu.h>

class wxMenu;

class QemuUIMenuBar : public wxMenuBar
{
public:
	QemuUIMenuBar();

	wxMenu* GetFileMenu();
	/**
	 * Disables all vm related menu entries an gets called if when
	 * the homepage is activated.
	 */ 
	void Disable();
	void Settings(bool state);
	void SendCtrlAltDel(bool state);
	void PowerOn(bool state);
	void Pause(bool state);
	void PowerOff(bool state);
	void Reset(bool state);
	/**
	 * places the exit menuitem at the end of the menu gets
	 * called after a filehistory update occured
	 */
	void UpdateFileHistory(const size_t fileHistoryCount);
	/**
	 * used to enable or disable toolbar related menuentries
	 * gets called when the toolbar is hidden/shown
	 */
	void EnableToolbar(bool);

	/**
	 * used to reflect the current state of the favorites panel
	 * gets called when the splitter is unsplit
	 */
	void CheckFavorites(bool);

	/**
	 * used to reflect the changes of the currently open vm's
	 * in the window menu
	 */
	void UpdateWindowMenu(const size_t posOld,const size_t posNew);
	void RemoveWindowMenuEntry(const size_t pos);
	void PrependWindowMenuEntry(const wxString& title);
	void AppendWindowMenuEntry(const wxString& title);
	void CheckWindowMenuEntry(size_t pos);
	/**
	 * returns the position of the currently checked menu entry
	 */
	int GetCheckedWindowMenuEntry();
private:
	wxMenu *CreateFileMenu();
	wxMenu *CreateViewMenu();
	wxMenu *CreateVMMenu();
	wxMenu *CreatePowerMenu();
	wxMenu *CreateWindowMenu();
	wxMenu *CreateHelpMenu();
	wxMenu *filemenu;
	wxMenu *viewmenu;
	wxMenu *vmmenu;
	wxMenu *powermenu;
	wxMenu *windowmenu;
	wxMenu *helpmenu;

	void InsertWindowMenuEntry(const size_t pos,const wxString& title);
};

#endif
