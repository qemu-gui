#include <wx/wx.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include "mainframe.h"
#include "qemu-ui.h"
#include "pipedprocess.h"
#include "socket.h"
#include "console.h"
#include "vm.h"

BEGIN_EVENT_TABLE(ConsolePanel, wxPanel)
    EVT_COMMAND (wxID_ANY,EVT_PROCESS_INPUT,ConsolePanel::HandleConsoleInput)
    EVT_COMMAND (wxID_ANY,EVT_PROCESS_OUTPUT,ConsolePanel::HandleConsoleOutput)
    EVT_COMMAND (wxID_ANY,EVT_PROCESS_CREATED,ConsolePanel::HandleProcessCreation)
    EVT_COMMAND (wxID_ANY,EVT_PROCESS_TERMINATED,ConsolePanel::HandleProcessTermination)
    EVT_COMMAND (wxID_ANY,EVT_SOCKET_INPUT,ConsolePanel::HandleConsoleInput)
    EVT_COMMAND (wxID_ANY,EVT_SOCKET_WRITE,ConsolePanel::HandleConsoleOutput)
END_EVENT_TABLE()

ConsolePanel::ConsolePanel(wxWindow *parent,QemuVM *vm) : wxPanel(parent){
	this->vm = vm;
	this->process = vm->GetProcess();
	this->socket = vm->GetSocket();

	if(process)
		process->AddEventHandler(this);

	if(socket)
		socket->AddEventHandler(this);

	console = new wxTextCtrl(this,-1,wxT(""),wxDefaultPosition,
	                         wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY|
	                         wxHSCROLL|wxTE_LINEWRAP
	);

	console->SetFont(wxFont(10, wxFONTFAMILY_TELETYPE,wxFONTSTYLE_NORMAL,
	                        wxFONTWEIGHT_NORMAL));
	
	input = new InputTextCtrl(this);

	sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(console, 1, wxEXPAND|wxADJUST_MINSIZE, 0);
	sizer->Add(input, 0, wxEXPAND|wxADJUST_MINSIZE, 0);
	SetAutoLayout(true);
	SetSizer(sizer);
}

ConsolePanel::~ConsolePanel(){
}

void ConsolePanel::SetProcess(PipedProcess *process){
	if(!this->process){
		this->process = process;
		process->AddEventHandler(this);
	}
}

bool ConsolePanel::Notify(const wxString& cmd){
	if(socket != NULL){
		// will generate an event which is handlet
		// by HandleConsoleOutput
		socket->Write(cmd);
	}

	return true;
}

void ConsolePanel::HandleConsoleInput( wxCommandEvent &event )
{
	AppendConsoleOutput(event.GetString());
}

void ConsolePanel::HandleConsoleOutput( wxCommandEvent &event )
{
	AppendConsoleInput(event.GetString());
}

void ConsolePanel::HandleProcessCreation(wxCommandEvent& event){
	console->Clear();
	AppendConsoleInput(event.GetString());
}
	
void ConsolePanel::HandleProcessTermination(wxCommandEvent& event)
{
	wxString msg;
	msg << _("Qemu exited with");
	msg << wxT(" ") << event.GetInt() << wxT(".");
	AppendConsoleError(msg);
	QemuGUI::GetMainFrame()->UpdateState();
}

void ConsolePanel::AppendConsoleInput(const wxString& cmd){
	wxTextAttr attr;
	attr.SetFont(wxFont(10, wxFONTFAMILY_TELETYPE,wxFONTSTYLE_NORMAL,
	                    wxFONTWEIGHT_BOLD));
	long start, end;
	start = console->GetLastPosition();
	console->AppendText(wxString::Format(wxT(">>%s\n"),cmd.c_str()));
	end = console->GetLastPosition();
	console->SetStyle(start, end, attr);
	ScrollToEnd();
}

void ConsolePanel::AppendConsoleOutput(const wxString& cmd){
	console->AppendText(cmd + wxT("\n"));
	ScrollToEnd();
}

void ConsolePanel::AppendConsoleError(const wxString& cmd){
	console->AppendText(cmd + wxT("\n"));
	ScrollToEnd();
}

void ConsolePanel::ScrollToEnd(){
	console->ShowPosition(console->GetLastPosition());
}

void ConsolePanel::SetState(bool state){
	input->Enable(state);
}

void InputTextCtrl::OnKeyDown(wxKeyEvent& event){
	switch(event.GetKeyCode()){
		case WXK_RETURN:
		case WXK_NUMPAD_ENTER:
			const wxString& cmd = this->GetValue();
			if(((ConsolePanel *)GetParent())->Notify(cmd))
				this->Clear();
			break;
	}

	event.Skip();
}

InputTextCtrl::InputTextCtrl(ConsolePanel *parent)
              :wxTextCtrl(parent, wxID_ANY, wxT(""), wxDefaultPosition, 
                          wxDefaultSize, wxTE_PROCESS_ENTER|wxTE_PROCESS_TAB){
	
}

BEGIN_EVENT_TABLE(InputTextCtrl, wxTextCtrl)
    EVT_KEY_DOWN(InputTextCtrl::OnKeyDown)
END_EVENT_TABLE()
