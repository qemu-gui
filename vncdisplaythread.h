#ifndef QEMU_GUI_VNC_DISPLAY_THREAD_H
#define QEMU_GUI_VNC_DISPLAY_THREAD_H

#include <wx/thread.h>
class VNCPanel;

class VNCDisplayThread : public wxThread {
public:
    /**
     * Creates a new VNCDisplayThread.
     *
     * @param panel The VNCPanel to talk to.
     */
    VNCDisplayThread(VNCPanel *panel);
    
    /**
     * Stops this thread.
     */
    void Stop();

private:
    VNCPanel *panel;
    bool running;

    /**
     * The thread code. Called by wxThread::Run().
     */
    void *Entry();
    
};

#endif
