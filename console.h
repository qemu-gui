#ifndef QEMU_GUI_CONSOLE_H
#define QEMU_GUI_CONSOLE_H

#include <wx/textctrl.h>
#include "notebook.h"

class wxTextCtrl;
class wxProcessEvent;
class wxInputStream;
class wxOutputStream;
class wxBoxSizer;

class QemuVM;
class MonitorSocket;
class PipedProcess;
class InputTextCtrl;

class ConsolePanel : public wxPanel {
public:
	/**
	 * Creates a new ConsolePanel.
	 *
	 * @param parent The wxWindow parent.
	 */
	ConsolePanel(wxWindow *parent, QemuVM *vm);
    
	/**
	 * Destructs this ConsolePanel.
	 */
	~ConsolePanel();

	/**
	 * Gets called if new input is entered, if the return value
	 * evaluates to true then the input box is cleared otherwhise
	 * not.
	 */
	bool Notify(const wxString&);
	
	/**
	 * Set the process whose output should be captured
	 */
	void SetProcess(PipedProcess *process);

	/**
	 * Enable/Disables the input box
	 */
	void SetState(bool);
	void ScrollToEnd();
private:
	/**
	 * Text control which displays the console output
	 */
	wxTextCtrl* console;

	/**
	 * Text control which takes the input
	 */
	InputTextCtrl* input;

	/**
	 * Sizer to control the size of the widgets
	 */
	wxBoxSizer* sizer;

	QemuVM *vm;
	MonitorSocket *socket;

	PipedProcess *process;
	wxOutputStream *out;
	wxInputStream *in;
	wxInputStream *err;

	/**
	 * adds the text to the console when the user has pressed
	 * enter and sends it to the associated process
	 */
	void AppendConsoleInput(const wxString&);
	/**
	 * displays whatever the captured process sends to its
	 * stdout/stderr
	 */ 
	void AppendConsoleOutput(const wxString&);
	void AppendConsoleError(const wxString&);
	/**
	 * event handlers which are connected to the piped process
	 */
	void HandleConsoleInput(wxCommandEvent&);
	void HandleConsoleOutput(wxCommandEvent&);
	void HandleProcessCreation(wxCommandEvent&);
	void HandleProcessTermination(wxCommandEvent&);
    
	DECLARE_EVENT_TABLE()	
};

class InputTextCtrl : public wxTextCtrl
{
public:
	InputTextCtrl(ConsolePanel *parent);
	void OnKeyDown(wxKeyEvent& event);
private:
	DECLARE_EVENT_TABLE()
};

#endif
