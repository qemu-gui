#ifndef QEMU_GUI_FAVORITES
#define QEMU_GUI_FAVORITES

#include <wx/panel.h>
//#include <wx/menu.h>
#include <wx/listctrl.h>

class wxButton;
class wxStaticText;
class wxBoxSizer;
class QemuVM;

class QemuUIFavoritesWindow : public wxPanel
{
public:
	QemuUIFavoritesWindow(wxWindow *parent);
	~QemuUIFavoritesWindow();
	void AddVM(QemuVM *vm);
private:
	void LoadFavorites();
	void OnListItemActivated(wxListEvent&);
	void OnContextMenu(wxListEvent&);
	void OnContextMenuOpen(wxCommandEvent&);
	void OnContextMenuEdit(wxCommandEvent&);
	void OnContextMenuRemove(wxCommandEvent&);
	void OpenVM();
	wxStaticText *label;
	wxButton *button;
	wxListCtrl *list;
	wxBoxSizer *sizer;
	DECLARE_EVENT_TABLE()
};

enum {
	ID_FAVORITES_CLOSE = 11112,
	ID_FAVORITES_CONTEXT_OPEN,
	ID_FAVORITES_CONTEXT_EDIT,
	ID_FAVORITES_CONTEXT_REMOVE
};

#endif
