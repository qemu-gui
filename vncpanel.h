#ifndef QEMU_GUI_VNCPANEL_H
#define QEMU_GUI_VNCPANEL_H

#include <wx/scrolwin.h>
#include <wx/dcbuffer.h>
#include <wx/thread.h>

class VNCDisplayThread;

extern "C" {
	#include <rfb/rfbclient.h>
}

class VNCPanel : public wxScrolledWindow {
public:
	VNCPanel(wxWindow *parent,wxString host,int display);
	~VNCPanel();

	/**
	 * trys to establish a connection to the given host/display
	 */
	bool Connect(int display);
	bool Connect(wxString host,int display);
	/**
	 * stops the vncdisplaythread relases memory used by the 
	 * vncclient, gets called from the destructor.
	 */
	void Disconnect();
	void HandleProcessTermination(wxCommandEvent&);
	/**
	 * paints the contents of the SDL surface to the wxPanel
	 */
	void Paint();

	/**
	 * return the VNCClient
	 */
	
	rfbClient* GetVNCClient();

	/**
	 * draws the updates requested by VLC to the wxPanel
	 */
	
	void VNCUpdateRect(int x,int y,int w,int h);
   
   	/**
	 * handles screen resizes and creates/allocates the frame buffer
	 */
	
	rfbBool VNCResize();

private:
	wxBitmap* VNCGenerateBitmap(int x,int y,int w,int h);
	/**
	 * Called to paint the panel.
	 *
	 * @param event The triggering wxPaintEvent (unused).
	 */
	void OnPaint(wxPaintEvent &event);
    
	/**
	 * Called to erase the background.
	 *
	 * @param event The triggering wxEraseEvent (unused).
	 */
	void OnEraseBackground(wxEraseEvent &event);

	/**
	 * Captures all mouse events and sends them to the vncserver
	 */
	void OnMouseEvent(wxMouseEvent& event);
	void HideCursor();
	void ShowCursor();
	/**
	 * Captures all key events and sends them to the vncserver
	 */
	void OnKeyDown(wxKeyEvent& event);
	void OnKeyUp(wxKeyEvent& event);
	rfbKeySym TranslateKeyCode(wxKeyEvent& event);

	bool Lock();
	void UnLock();

	rfbClient *vncclient;
	VNCDisplayThread *vncdisplaythread;
	bool locked;

	DECLARE_EVENT_TABLE()
};

#endif
