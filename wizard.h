#ifndef QEMU_GUI_NEW_VM_WIZARD_H
#define QEMU_GUI_NEW_VM_WIZARD_H

#include <wx/wizard.h>

class NewVMNameWizardPage;
class QemuVM;

class QemuUINewVMWizard : public wxWizard
{
public:
	QemuUINewVMWizard(wxFrame*);
	QemuVM* RunIt();
	void OnWizardCancel(wxWizardEvent&);

private:
	wxWizardPageSimple *welcome;
	NewVMNameWizardPage *page1;
	DECLARE_EVENT_TABLE();
};

#endif
