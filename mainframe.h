#ifndef QEMU_GUI_MAINFRAME_H
#define QEMU_GUI_MAINFRAME_H

#include <wx/splitter.h>
#include <wx/notebook.h>
#include <wx/dynarray.h>
#include <wx/frame.h>

class VNCPanel;

class QemuVM;
class QemuUIMenuBar;
class QemuUIToolBar;
class QemuUINotebook;
class QemuUINotebookPage;
class QemuUIStatusBar;
class wxConfigBase;
class wxScrolledWindow;
class wxFileHistory;

WX_DEFINE_ARRAY_PTR(QemuVM *,VMArray);

class QemuMainFrame : public wxFrame
{
public:
	QemuMainFrame(const wxString& title);

	/**
	 * shows the homepage by selecting the corresponding tab
	 * or calls CreateHomepage() to create a new one.
	 */
	void ShowHomepage();
	/**
	 * returns the currently active vm tab or NULL if the homepage
	 * is active
	 */
	QemuUINotebookPage* GetCurrentVMTab();
	/**
	 * returns the currently active virtual machine or NULL
	 */
	QemuVM* GetCurrentVM();

	/**
	 * opens a vm, creates a new tab and updates the vms array,
	 * returns false if the vm is already open.
	 */
	bool OpenVM(QemuVM *vm);
	void OpenVM(const wxString& config);
	//void CloseVM(QemuVM *vm);

	void SaveFileHistory(wxConfigBase*);
	void LoadFileHistory(wxConfigBase*);
		
	/**
	 * enables / disables the toolbar buttons and the statusbar
	 * according to the vm state
	 */
	void UpdateState();


	/**
	 * gets called when a radio item from the windows menu is selected
	 * has to be public because the event handler is dynamically added
	 * in menubar.cpp
	 */
	void OnChangeWindow(wxCommandEvent&);
private:
	/* various event handlers */
	void OnNewVM(wxCommandEvent &event);
    	void OnOpenVM(wxCommandEvent &event);
    	void OnQuitCommandEvent(wxCommandEvent& event);
    	/**
	 * quit's the application, checks if there are still vm running
	 * and if so asks the user what to do.
	 */
	void OnQuit(wxCloseEvent& event);
    
    	void OnShowHomepage(wxCommandEvent& event);
    	void OnFullScreen(wxCommandEvent& event);
    	void OnToggleToolBar(wxCommandEvent& event);
    	void OnToolBarSettingChange(wxCommandEvent& event);
    	void OnToggleStatusBar(wxCommandEvent& event);

	void OnVMSettings(wxCommandEvent& event);
	void OnVMSendCtrlAltDel(wxCommandEvent& event);
	
	void OnPowerOnVM(wxCommandEvent& event);
	void OnPowerOffVM(wxCommandEvent& event);
	void OnPauseVM(wxCommandEvent& event);
	void OnResetVM(wxCommandEvent& event);
	void OnKillVM(wxCommandEvent& event);

	void OnSnapshot(wxCommandEvent& event);
	void OnRevertSnapshot(wxCommandEvent& event);

	void OnToggleDisplay(wxCommandEvent& event);
	
	void OnSendCommandToVM(wxCommandEvent& event);

	void OnNextWindow(wxCommandEvent&);
	void OnPrevWindow(wxCommandEvent&);
	/**
	 * notebook related event handlers
	 */
	void OnNotebookPageChanged(wxNotebookEvent& event);
	void OnNotebookPageClosed(wxCommandEvent&);
	void CloseNotebookPage(const int pos);

	void OnConnectCdRom(wxCommandEvent&);
	
	/**
	 * creates the homepage tab and prepends an entry to the
	 * window menu
	 */
	void CreateHomepage();

	/**
	 * gets called from the filehistory menu items
	 */
	void OnReOpenVM(wxCommandEvent& event);
	
	void OnOpenQemuDoc(wxCommandEvent& event);
    	void OnAbout(wxCommandEvent& event);

	/**
	 * Sets up some sizers which make sure that the notebook
	 * widget takes all available space.
	 */
	void DoLayout();
	
	QemuUIMenuBar *menubar;
	QemuUIToolBar *toolbar;
	QemuUIStatusBar *statusbar;

	wxScrolledWindow *mainwindow;
	QemuUINotebook *notebook;

	wxFileHistory *filehistory;

	// holds all currently opened vm's
	VMArray vms;
	// used to store the current toolbar style before toogling
	long toolbarStyle;

	DECLARE_EVENT_TABLE()
};

#endif
