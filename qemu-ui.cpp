/*
 * Copyright (C) 2006 Marc Andr� Tanner <mat@brain-dump.org>
 *
 * qemu-gui is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * qemu-gui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with qemu-gui; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * QEMU is a trademark of Fabrice Bellard
 */

#include <wx/wx.h>
#include <wx/config.h>
#include <wx/fileconf.h>
#include <wx/image.h>
#include "qemu-ui.h"
#include "mainframe.h"
#include "config.h"

IMPLEMENT_APP(QemuGUI)

QemuMainFrame *QemuGUI::frame;

bool QemuGUI::OnInit()
{
	// init all image handlers otherwhise the images referenced
	// in welcome.html wouldn't be displayed correctly.
	wxInitAllImageHandlers();
	
	// set up a config object which will be used to store things
	// like favorites or the filehistory
	wxFileConfig *config = new wxFileConfig(wxT("qemu-ui"));
	wxConfigBase::Set(config);
	
	frame = new QemuMainFrame(_T("QEMU GUI"));
	frame->LoadFileHistory(wxConfigBase::Get());
	frame->Centre();
	frame->Show();
	SetTopWindow(frame);
	return true;
}

QemuMainFrame* QemuGUI::GetMainFrame(){
	return frame;
}

int QemuGUI::OnRun() {
	// start the main loop
	return wxApp::OnRun();
}

int QemuGUI::OnExit() {
	frame->SaveFileHistory(wxConfigBase::Get());
	
	// Set() returns the active config object as Get() does, but unlike
	// Get() it doesn't try to create one if there is none (definitely 
	// not what we want here!)
	delete wxConfigBase::Set((wxConfigBase *) NULL); 
	
	// return the standard exit code
	return wxApp::OnExit();
}
