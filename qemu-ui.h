#ifndef QEMU_GUI_H
#define QEMU_GUI_H

#include <wx/app.h>

class QemuMainFrame;
class VNCDisplayThread;

class QemuGUI : public wxApp
{
public:
	bool OnInit();
	int OnRun();
	int OnExit();
	static QemuMainFrame* GetMainFrame();
private:
	static QemuMainFrame *frame;
	VNCDisplayThread *thread;
};

#endif
