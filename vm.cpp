#include "vm.h"
#include "socket.h"
#include <wx/wx.h>
#include <wx/filename.h>

QemuVM::QemuVM(const wxString& configfile) : wxEvtHandler(){
	this->process = NULL;
	this->socket = new MonitorSocket();
	this->state = VM_STATE_POWERED_OFF;
	this->pid = -1;
	this->vncdisplay = -1;
	this->monitorport = -1;
	this->configfile = configfile;

	if(!wxFileExists(configfile)){
		wxFile file;
		file.Create(configfile);
		file.Close();
	}

	wxFileInputStream stream(configfile);
	config = new wxFileConfig(stream);
}

QemuVM::~QemuVM(){
	delete this->config;
}

int QemuVM::vncdisplays = 10;
int QemuVM::monitorports = 3000;

wxString QemuVM::SendCommand(const wxString& cmd, bool block){
	this->socket->Write(cmd,block);
	if(block)
		return socket->ReadAll();
	else
		return wxEmptyString;
}

wxString QemuVM::GetTitle(){
	return config->Read(wxT("/vm/title"));
}

void QemuVM::SetTitle(const wxString& title){
	config->Write(wxT("/vm/title"),title);
}

wxString QemuVM::GetCmdlineArguments(){
	return config->Read(wxT("/vm/cmdline"));
}

void QemuVM::SetCmdlineArguments(const wxString& args){
	config->Write(wxT("/vm/cmdline"),args);
}

wxString QemuVM::GetConfigFile(){
	return configfile;
}

void QemuVM::SaveConfig(){
	this->SaveConfig(configfile);
}

void QemuVM::SaveConfig(const wxString& configfile){
	wxFileOutputStream stream(configfile);
	config->Save(stream);
}

MonitorSocket *QemuVM::GetSocket(){
	return this->socket;
}

PipedProcess *QemuVM::GetProcess(){
	if(!process){
		process = new PipedProcess();
		process->AddEventHandler(this);
	}
	return process;
}

void QemuVM::AddEventHandler(wxEvtHandler *handler){
	process->AddEventHandler(handler);
}

long QemuVM::PowerOn(){
	if(!process)
		return -1;

	if(state == VM_STATE_PAUSED){
		Continue();
		return pid;
	}

	wxString cmd = wxT("qemu");
	cmd << GetCmdline();
	pid = process->Launch(cmd,1000);

	// give some time to start up
	wxMilliSleep(500);

	if(!socket->Connect(wxT("localhost"),GetCurrentMonitorPort())){	
		return -1;
	}
	state = VM_STATE_RUNNING;
	return pid;
}

void QemuVM::PowerOff(){
	//SendCommand(wxT("system_powerdown"));
	SendCommand(wxT("quit"));
	state = VM_STATE_POWERED_OFF;
}

void QemuVM::Kill(){
	if(!process || pid == -1)
		return;
	if(wxProcess::Exists(pid)){
		if(wxKill(pid,wxSIGKILL) == 0){
			state = VM_STATE_POWERED_OFF;
			pid = -1;
		}
	}
}

void QemuVM::Reset(){
	if(!process)
		return;

	SendCommand(wxT("system_reset"));
	state = VM_STATE_RUNNING;
}

void QemuVM::Pause(){
	if(!process)
		return;
	
	SendCommand(wxT("stop"));
	state = VM_STATE_PAUSED;
}

void QemuVM::Continue(){
	if(!process)
		return;
	
	SendCommand(wxT("cont"));
	state = VM_STATE_RUNNING;
}

void QemuVM::Suspend(){
	if(!process)
		return;
	
	//SendCommand(wxT("savevm supended"));
	SendCommand(wxT("stop"));
	state = VM_STATE_SUSPENDED;
}

void QemuVM::Snapshot(){
	if(!process)
		return;
	SendCommand(wxT("savevm snapshot"));
	config->Write(wxT("/snapshots/snapshot"),wxT("snapshot"));
}

void QemuVM::Revert(){
	if(!process)
		return;
	SendCommand(wxT("loadvm snapshot"));
	state = VM_STATE_RUNNING;
	config->DeleteEntry(wxT("/snapshots/snapshot"));
}

bool QemuVM::HasSnapshot(){
	wxString snapshot = config->Read(wxT("/snapshots/snapshot"));
	return !snapshot.IsEmpty();
}

bool QemuVM::IsRunning(){
	return state == VM_STATE_RUNNING;
}

bool QemuVM::IsPaused(){
	return state == VM_STATE_PAUSED;
}

bool QemuVM::IsPoweredOff(){
	return state == VM_STATE_POWERED_OFF;
}

void QemuVM::OnProcessTermination(wxCommandEvent& WXUNUSED(event)){
	state = VM_STATE_POWERED_OFF;
	pid = -1;
}

wxString QemuVM::GetHd(const int hd){
	wxString device = wxString::Format(wxT("/devices/hd%c"),(char)('a'+hd));
	return config->Read(device);
}

void QemuVM::SetHd(const int hd,const wxString& image){
	wxString device = wxString::Format(wxT("/devices/hd%c"),(char)('a'+hd));
	config->Write(device,image);
}

wxString QemuVM::GetCdRom(){
	return config->Read(wxT("/devices/cdrom"));
}

void QemuVM::SetCdRom(const wxString& image){
	config->Write(wxT("/devices/cdrom"),image);
}

wxString QemuVM::GetFd(const int fd){
	wxString device = wxString::Format(wxT("/devices/fd%c"),(char)('a'+fd));
	return config->Read(device);
}

void QemuVM::SetFd(const int fd,const wxString& image){
	wxString device = wxString::Format(wxT("/devices/fd%c"),(char)('a'+fd));
	config->Write(device,image);
}

int QemuVM::GetMemory(){
	return config->Read(wxT("/devices/memory"),0L);
}

void QemuVM::SetMemory(const int memory){
	if(memory != 0)
		config->Write(wxT("/devices/memory"),memory);
}

int QemuVM::GetSmp(){
	return config->Read(wxT("/cpu/smp"),1L);
}

void QemuVM::SetSmp(const int smp){
	config->Write(wxT("/cpu/smp"),smp);
}

wxString QemuVM::GetSMB(){
	return config->Read(wxT("/net/smb"));
}

void QemuVM::SetSMB(const wxString& smb){
	if(!smb.IsEmpty())
		config->Write(wxT("/net/smb"),smb);
	else
		config->DeleteEntry(wxT("/net/smb"),true);
}

wxString QemuVM::GetTFTP(){
	return config->Read(wxT("/net/tftp"));
}

void QemuVM::SetTFTP(const wxString& tftp){
	if(!tftp.IsEmpty())
		config->Write(wxT("/net/tftp"),tftp);
	else
		config->DeleteEntry(wxT("/net/tftp"),true);
}

wxString QemuVM::GetBoot(){
	return config->Read(wxT("/vm/boot"));
}

void QemuVM::SetBoot(const wxString& boot){
	config->Write(wxT("/vm/boot"),boot);
}

wxString QemuVM::GetKeyboardLayout(){
	return config->Read(wxT("/devices/keyboard"),wxT("en-us"));
}

void QemuVM::SetKeyboardLayout(const wxString& layout){
	config->Write(wxT("/devices/keyboard"),layout);
}

void QemuVM::ConnectCdRom(bool state){
	if(state){
		SendCommand(wxT("connect cdrom"));
	} else {
		SendCommand(wxT("disconnect cdrom"));
	}
}

bool QemuVM::IsCdRomConnected(){
	return true;
}

bool QemuVM::GetWin2kHack(){
	wxString win2khack = config->Read(wxT("/misc/win2k-hack"));
	return !win2khack.Cmp(wxT("true"));
}

void QemuVM::SetWin2kHack(bool win2khack){
	config->Write(wxT("/misc/win2k-hack"),win2khack ? wxT("true") : wxT("false"));
}

bool QemuVM::GetLocalTime(){
	wxString localtime = config->Read(wxT("/misc/localtime"));
	return !localtime.Cmp(wxT("true"));
}

void QemuVM::SetLocalTime(bool localtime){
	config->Write(wxT("/misc/localtime"),localtime ? wxT("true") : wxT("false"));
}

bool QemuVM::GetSnapshotMode(){
	wxString mode = config->Read(wxT("/snapshot/snapshotmode"));
	return !mode.Cmp(wxT("true"));
}

void QemuVM::SetSnapshotMode(bool mode){
	config->Write(wxT("/snapshot/snapshotmode"),mode ? wxT("true"): wxT("false"));
}

bool QemuVM::GetStdVga(){
	wxString vga = config->Read(wxT("/misc/std-vga"));
	return !vga.Cmp(wxT("true"));
}

void QemuVM::SetStdVga(bool vga){
	config->Write(wxT("/misc/std-vga"),vga ? wxT("true") : wxT("false"));
}

bool QemuVM::GetNoACPI(){
	wxString acpi = config->Read(wxT("/misc/no-acpi"));
	return !acpi.Cmp(wxT("true"));
}

void QemuVM::SetNoACPI(bool acpi){
	config->Write(wxT("/misc/no-acpi"),acpi ? wxT("true") : wxT("false"));
}

int QemuVM::GetCurrentMonitorPort(){
	return this->monitorport;
}

int QemuVM::GetMonitorPort(){
	return config->Read(wxT("/misc/monitorport"),-1L);
}

void QemuVM::SetMonitorPort(const int port){
	if(port > 0)
		config->Write(wxT("/misc/monitorport"),port);
}

int QemuVM::GetCurrentVNCDisplay(){
	return this->vncdisplay;
}

int QemuVM::GetVNCDisplay(){
	return config->Read(wxT("/misc/vncdisplay"),-1L);
}

void QemuVM::SetVNCDisplay(const int display){
	if(display > 0)
		config->Write(wxT("/misc/vncdisplay"),display);
}

wxString QemuVM::GetCmdline(){
	wxString cmdline = wxT(" -monitor ");
	cmdline << wxT(" tcp::");
		
	if(monitorport >= 0)
		cmdline << monitorport;
	else if((monitorport = GetMonitorPort()) >= 0)
		cmdline << monitorport;
	else 
		cmdline << (monitorport = QemuVM::monitorports++);

	cmdline << wxT(",server ");

	cmdline << wxT(" -usbdevice tablet -vnc :");

	if(vncdisplay >= 0)
		cmdline << vncdisplay;
	else if((vncdisplay = GetVNCDisplay()) >= 0)
		cmdline << vncdisplay;
	else 
		cmdline << (vncdisplay = QemuVM::vncdisplays++);

	wxString hda = GetHd(0);
	if(!hda.IsEmpty())
		cmdline << wxT(" -hda ") << hda;
	wxString hdb = GetHd(1);
	if(!hdb.IsEmpty())
		cmdline << wxT(" -hdb ") << hdb;
	wxString hdc = GetHd(2);
	wxString cdrom = GetCdRom();
	if(!hdc.IsEmpty() && cdrom.IsEmpty())
		cmdline << wxT(" -hdc ") << hdc;
	wxString hdd = GetHd(3);
	if(!hdd.IsEmpty())
		cmdline << wxT(" -hdd ") << hdd;
	if(!cdrom.IsEmpty())
		cmdline << wxT(" -cdrom ") << cdrom;

	wxString fda = GetFd(0);
	if(!fda.IsEmpty())
		cmdline << wxT(" -fda ") << fda;
	wxString fdb = GetFd(1);
	if(!fdb.IsEmpty())
		cmdline << wxT(" -fdb ") << fdb;

	wxString boot = GetBoot();
	if(!boot.IsEmpty())
		cmdline << wxT(" -boot ") << boot;
	
	const int memory = GetMemory();
	if(memory != 0)
		cmdline << wxT(" -m ") << memory;

	int smp = GetSmp();
	if(smp != 1)
		cmdline << wxT(" -smp ") << smp;

	wxString smb = GetSMB();
	if(!smb.IsEmpty())
		cmdline << wxT(" -smb ") << smb;

	wxString tftp = GetTFTP();
	if(!tftp.IsEmpty())
		cmdline << wxT(" -tftp ") << tftp;

	if(GetLocalTime())
		cmdline << wxT(" -localtime ");
	
	if(GetWin2kHack())
		cmdline << wxT(" -win2k-hack ");
	
	if(GetStdVga())
		cmdline << wxT(" -std-vga ");

	if(GetNoACPI())
		cmdline << wxT(" -no-acpi ");

	if(GetSnapshotMode())
		cmdline << wxT(" -snapshot ");

	wxString keyboard = GetKeyboardLayout();
	if(!keyboard.IsEmpty())
		cmdline << wxT(" -k ") << keyboard;
	
	cmdline << wxT(" ") << GetCmdlineArguments();
	return cmdline;
}

wxString QemuVM::GetPath(){
	wxFileName filename(this->configfile);
	return filename.GetPath(wxPATH_GET_VOLUME | wxPATH_GET_SEPARATOR);
}

long QemuVM::GetPID(){
	return this->pid;
}

void QemuVM::SendKey(const wxString& key){
	SendCommand(wxT("sendkey ") + key);
}

BEGIN_EVENT_TABLE(QemuVM, wxEvtHandler)
	EVT_COMMAND(wxID_ANY,EVT_PROCESS_TERMINATED,QemuVM::OnProcessTermination)
END_EVENT_TABLE()
