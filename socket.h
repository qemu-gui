#ifndef QEMU_GUI_SOCKET
#define QEMU_GUI_SOCKET

#include "events.h"

class wxEvtHandler;
class wxSocketEvent;
class wxSocketClient;

DECLARE_EVENT_TYPE(EVT_SOCKET_INPUT,wxID_ANY)
DECLARE_EVENT_TYPE(EVT_SOCKET_WRITE,wxID_ANY)
DECLARE_EVENT_TYPE(EVT_SOCKET_LOST,wxID_ANY)
//WX_DEFINE_ARRAY_PTR(wxEvtHandler *,EvtHandlerArray);

class MonitorSocket : public wxEvtHandler {
public:
	MonitorSocket();
	bool Connect(const wxString& host, const unsigned int port);
	void Disconnect();
	void Write(const wxString& cmd, bool block = false);
	wxString Read();
	wxString ReadAll();
	void OnSocketEvent(wxSocketEvent& event);
	void AddEventHandler(wxEvtHandler *handler);
private:
	void SendEvent(wxCommandEvent& event);
	wxSocketClient* socket;
	wxString data;
	EvtHandlerArray evtHandlers;
	DECLARE_EVENT_TABLE()
};

#endif
