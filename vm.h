#ifndef QEMU_GUI_VM
#define QEMU_GUI_VM

#include <wx/wfstream.h>
#include <wx/fileconf.h>
#include "pipedprocess.h"

class MonitorSocket;

class QemuVM : public wxEvtHandler {
public:
	QemuVM(const wxString& configfile);
	~QemuVM();
	wxString GetTitle();
	void SetTitle(const wxString&);
	wxString GetCmdline();
	wxString GetCmdlineArguments();
	void SetCmdlineArguments(const wxString&);
	
	/**
	 * returns the associated config file of this vm
	 */
	wxString GetConfigFile();
	wxString GetPath();
	void SaveConfig(const wxString& configfile);
	void SaveConfig();

	long PowerOn();
	void Suspend();
	void Pause();
	void Continue();
	void Reset();
	void PowerOff();
	void Kill();
	bool IsRunning();
	bool IsPaused();
	bool IsPoweredOff();
	void Snapshot();
	void Revert();
	bool HasSnapshot();
	
	/**
	 * 0=>hda,1=>hdb ...
	 */
	wxString GetHd(const int hd);
	void SetHd(const int hd,const wxString& image);
	wxString GetCdRom();
	void SetCdRom(const wxString& image);
	wxString GetFd(const int fd);
	void SetFd(const int fd,const wxString& image);
	int GetMemory();
	void SetMemory(const int memory);
	int GetSmp();
	void SetSmp(const int);
	wxString GetSMB();
	void SetSMB(const wxString&);
	wxString GetTFTP();
	void SetTFTP(const wxString&);
	wxString GetBoot();
	void SetBoot(const wxString& boot);
	bool GetWin2kHack();
	void SetWin2kHack(bool);
	bool GetLocalTime();
	void SetLocalTime(bool);
	bool GetStdVga();
	void SetStdVga(bool);
	bool GetNoACPI();
	void SetNoACPI(bool);
	bool GetSnapshotMode();
	void SetSnapshotMode(bool);
	wxString GetKeyboardLayout();
	void SetKeyboardLayout(const wxString&);
	int GetCurrentMonitorPort();
	int GetMonitorPort();
	void SetMonitorPort(const int port);
	/* get the vncdisplay which is currently used by the vm */
	int GetCurrentVNCDisplay();
	/* get the vncdisplay as stored in the config file */
	int GetVNCDisplay();
	void SetVNCDisplay(const int display);

	void ConnectCdRom(bool state);
	bool IsCdRomConnected();

	long GetPID();
	/**
	 * Returns the asociated qemu process or creates it
	 */
	PipedProcess *GetProcess();
	MonitorSocket *GetSocket();
	/**
	 * Adds an eventhandler to the underling PipedProcess
	 * the passed eventhandler will therefore receive
	 * process termination events and so on.
	 */
	void AddEventHandler(wxEvtHandler *handler);
	wxString SendCommand(const wxString& cmd,bool block = false);
	void SendKey(const wxString& key);
private:
	/**
	 * Is connected to the termination event of the associated process
	 */
	void OnProcessTermination(wxCommandEvent& WXUNUSED(event));

	wxString configfile;
	wxFileConfig *config;
	PipedProcess *process;
	long pid;
	MonitorSocket *socket;
	int vncdisplay;
	static int vncdisplays;
	int monitorport;
	static int monitorports;

	enum vmstates {
		VM_STATE_RUNNING,
		VM_STATE_PAUSED,
		VM_STATE_POWERED_OFF,
		VM_STATE_SUSPENDED
	};

	vmstates state;

	DECLARE_EVENT_TABLE()
};

#endif
