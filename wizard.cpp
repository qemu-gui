#include <wx/wx.h>
#include "wizard.h"
#include "vm.h"
#include "guibuilder.h"
#include "icons/wizard.xpm"

enum {
	ID_CHOOSE_DIRECTORY_BUTTON
};

class NewVMNameWizardPage : public wxWizardPageSimple
{
public:
	NewVMNameWizardPage(wxWizard *parent) : wxWizardPageSimple(parent){
/*
	title = new wxTextCtrl(this, -1, wxT(""), wxPoint(24,64), wxSize(361,21), 0, wxDefaultValidator, wxT("title"));

	wxStaticBox *WxStaticBox2 = new wxStaticBox(this, -1, wxT("Virtual machine title"), wxPoint(13,45), wxSize(390,55));

	wxButton *button = new wxButton(this, CHOOSE_DIRECTORY_BUTTON, wxT("Browse"), wxPoint(310,135), wxSize(75,25), 0, wxDefaultValidator, wxT("directoryButton"));

	directory = new wxTextCtrl(this, -1, wxT(""), wxPoint(24,136), wxSize(278,21), 0, wxDefaultValidator, wxT("directory"));

	wxStaticBox *WxStaticBox1 = new wxStaticBox(this, -1, wxT("Location"), wxPoint(13,118), wxSize(390,55));
*/
	wxGuiBuilder gb(wxString::FromAscii(
		"    V{                                             "
		"         H'Set your virtual machine title'{        "
		"            [----------------]+,1:title            "
		"         }+                                        "
		"                                                   "
		"         V'Select the operating system'{           "
		"            ( ) 'Microsoft Windows'<               "
		"            ( ) 'GNU/Linux'<                       "
		"            ( ) '*BSD'<                            "
		"            (*) 'Another'<                         "
		"         }+                                        "
		"                                                   "
		"         H'Set a directory'{                       "
		"            [------],1:directory ['Browse']:button "
		"         }+                                        "
		"    }                                              "
	));

	gb << wxGbTextCtrl(wxT("title"), &title, wxID_ANY);
	gb << wxGbTextCtrl(wxT("directory"), &directory, wxID_ANY);
	gb << wxGbButton(wxT("button"),NULL,ID_CHOOSE_DIRECTORY_BUTTON);
	if(gb.Build(this, wxID_ANY)){
		this->SetSizerAndFit(gb.GetSizer());
	}
}

void OnWizardPageChanging(wxWizardEvent& event)
{
	// if we go backwards don't check
	if(!event.GetDirection())
		return;

        if(!title->GetValue())
        {
		wxMessageBox(
			_("You need to specify a name for your virtual machine."),
			_("Required information missing"),
			wxICON_WARNING | wxOK,
			this
		);
		title->SetFocus();
		event.Veto();
		return;
	}

	if(!directory->GetValue())
        {
		wxMessageBox(
			_("You need to specify a directory for your virtual machine."),
			_("Required information missing"),
			wxICON_WARNING | wxOK, 
			this
		);
		directory->SetFocus();
		event.Veto();
		return;
	}

	if(!wxDirExists(directory->GetValue())){
		wxMessageBox(_("The specified directory doesn't exists."),
		             _("Error"),
			     wxICON_ERROR | wxOK, this);
		event.Veto();
		return;
	}
	
	wxFile f;
	if(!f.Create(GetFileName(),true)){
		wxMessageBox(_("The specified directory/file isn't writeable."),
		             _("Error"),
			     wxICON_ERROR | wxOK, this);
		event.Veto();
		return;
	}
}

void ChooseDirectory(wxCommandEvent& WXUNUSED(event)){
	wxString dirHome;
	wxGetHomeDir(&dirHome);

	wxDirDialog dialog(this, _("Choose a directory for your VM"),dirHome,
                           wxDD_DEFAULT_STYLE | wxDD_NEW_DIR_BUTTON);

	if (dialog.ShowModal() == wxID_OK)
	{
		directory->SetValue(dialog.GetPath());
	}
}

wxString GetTitle(){
	return title->GetValue();
}

wxString GetFileName(){
	wxString file;
	file = directory->GetValue() << wxT("/") << title->GetValue() << wxT(".qvmc");
	return file;
}

private:
	wxTextCtrl *title;
	wxTextCtrl *directory;
	DECLARE_EVENT_TABLE();
};

BEGIN_EVENT_TABLE(NewVMNameWizardPage,wxWizardPageSimple)
	EVT_BUTTON(ID_CHOOSE_DIRECTORY_BUTTON,NewVMNameWizardPage::ChooseDirectory)
	EVT_WIZARD_PAGE_CHANGING(wxID_ANY,NewVMNameWizardPage::OnWizardPageChanging)
END_EVENT_TABLE()

QemuUINewVMWizard::QemuUINewVMWizard(wxFrame *frame)
		  :wxWizard(frame,wxID_ANY,_("New Virtual Machine")){

	this->welcome = new wxWizardPageSimple(this,NULL,NULL,wxBitmap(QEMU_NEW_VM_WIZARD_IMAGE));

    /* wxStaticText *text = */ new wxStaticText(welcome, wxID_ANY,
             _("This wizard will guide you through the steps of creating \n"
               "a new virtual machine."),
             wxPoint(5,5),wxSize(390,100)
        );

	page1 = new NewVMNameWizardPage(this);
	wxWizardPageSimple::Chain(welcome,page1);	
	
	// allow the wizard to size itself around the pages
	this->GetPageAreaSizer()->Add(welcome);
	this->GetPageAreaSizer()->Add(page1);
}

void QemuUINewVMWizard::OnWizardCancel(wxWizardEvent& event)
{
	if(wxMessageBox(_("Do you really want to cancel?"), _("Question"),
	                wxICON_QUESTION | wxYES_NO, this) != wxYES ){
		event.Veto();
	}
}

QemuVM* QemuUINewVMWizard::RunIt(){
	if(RunWizard(this->welcome)){
		QemuVM *vm = new QemuVM(page1->GetFileName());
		vm->SetTitle(page1->GetTitle());
		vm->SaveConfig();
		return vm;
	}

	return NULL;
}

BEGIN_EVENT_TABLE(QemuUINewVMWizard, wxWizard)
	EVT_WIZARD_CANCEL(wxID_ANY, QemuUINewVMWizard::OnWizardCancel)
END_EVENT_TABLE()
