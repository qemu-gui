#include <wx/wx.h>
#include <wx/arrimpl.cpp>
#include <wx/docview.h>
#include "mainframe.h"
#include "wizard.h"
#include "qemu-ui.h"
#include "console.h"
#include "vm.h"
#include "config.h"
#include "notebook.h"
#include "menubar.h"
#include "toolbar.h"
#include "statusbar.h"
#include "vncpanel.h"
#include "vmdialog.h"
#include "events.h"

#if defined __WXDEBUG__
	#include <wx/debug.h>
	#include <wx/memory.h>
#endif

#if !defined(__WXMSW__) && !defined(__WXPM__)
    #include "icons/icon.xpm"
#endif

BEGIN_EVENT_TABLE(QemuMainFrame, wxFrame)
	EVT_MENU(QEMU_NEW, QemuMainFrame::OnNewVM)
	EVT_COMMAND(wxID_ANY,EVT_NEW_VM,QemuMainFrame::OnNewVM)
	EVT_MENU(QEMU_OPEN, QemuMainFrame::OnOpenVM)
	EVT_COMMAND(wxID_ANY,EVT_OPEN_VM,QemuMainFrame::OnOpenVM)
	EVT_MENU_RANGE(wxID_FILE1, wxID_FILE9, QemuMainFrame::OnReOpenVM)

	EVT_MENU(QEMU_CLOSE, QemuMainFrame::OnNotebookPageClosed)
	EVT_MENU(QEMU_QUIT, QemuMainFrame::OnQuitCommandEvent)
	EVT_CLOSE(QemuMainFrame::OnQuit)

	EVT_MENU(QEMU_HOMEPAGE, QemuMainFrame::OnShowHomepage)
	EVT_MENU(QEMU_FULLSCREEN, QemuMainFrame::OnFullScreen)
	EVT_MENU(QEMU_TOGGLE_TOOLBAR,  QemuMainFrame::OnToggleToolBar)
	EVT_MENU(QEMU_TOOLBAR_TEXT, QemuMainFrame::OnToolBarSettingChange)
	EVT_MENU(QEMU_TOOLBAR_ICONS, QemuMainFrame::OnToolBarSettingChange)
	EVT_MENU(QEMU_TOOLBAR_BOTH, QemuMainFrame::OnToolBarSettingChange)
	EVT_MENU(QEMU_TOGGLE_STATUSBAR,  QemuMainFrame::OnToggleStatusBar)

	EVT_MENU(QEMU_VM_SETTINGS,QemuMainFrame::OnVMSettings)
	EVT_MENU(QEMU_VM_SEND_CTRL_ALT_DEL,QemuMainFrame::OnVMSendCtrlAltDel)
	
	EVT_MENU(QEMU_VM_START,QemuMainFrame::OnPowerOnVM)
	EVT_MENU(QEMU_VM_PAUSE,QemuMainFrame::OnPauseVM)
	EVT_MENU(QEMU_VM_SHUTDOWN,QemuMainFrame::OnPowerOffVM)
	EVT_MENU(QEMU_VM_RESET,QemuMainFrame::OnResetVM)
	EVT_MENU(QEMU_VM_KILL,QemuMainFrame::OnKillVM)
	
	EVT_MENU(QEMU_VM_SNAPSHOT,QemuMainFrame::OnSnapshot)
	EVT_MENU(QEMU_VM_REVERT_SNAPSHOT,QemuMainFrame::OnRevertSnapshot)
	
	EVT_MENU(QEMU_TOOLBAR_DISPLAY_INFO,QemuMainFrame::OnToggleDisplay)
	EVT_MENU(QEMU_TOOLBAR_DISPLAY_SCREEN,QemuMainFrame::OnToggleDisplay)
	EVT_MENU(QEMU_TOOLBAR_DISPLAY_CONSOLE,QemuMainFrame::OnToggleDisplay)
	
	EVT_MENU(QEMU_TOOLBAR_SEND_VM_COMMAND,QemuMainFrame::OnSendCommandToVM)

	EVT_MENU(QEMU_WINDOW_NEXT, QemuMainFrame::OnNextWindow)
	EVT_MENU(QEMU_WINDOW_PREV, QemuMainFrame::OnPrevWindow)
	EVT_MENU(QEMU_DOC, QemuMainFrame::OnOpenQemuDoc)
	EVT_MENU(QEMU_ABOUT, QemuMainFrame::OnAbout)

	EVT_NOTEBOOK_PAGE_CHANGED(QEMU_VM_NOTEBOOK,QemuMainFrame::OnNotebookPageChanged)
	EVT_MENU(QEMU_VM_NOTEBOOK_CONTEXT_CLOSE,QemuMainFrame::OnNotebookPageClosed)
	EVT_MENU(ID_STATUSBAR_CDROM_CONNECT,QemuMainFrame::OnConnectCdRom)
END_EVENT_TABLE()

QemuMainFrame::QemuMainFrame(const wxString& title)
              :wxFrame(NULL, wxID_ANY, title)
{
	this->toolbarStyle = 0;
	SetIcon(wxICON(sample));

#if wxUSE_MENUS
	this->menubar = new QemuUIMenuBar();
	SetMenuBar(this->menubar);
	this->filehistory = new wxFileHistory(5);
	this->filehistory->UseMenu(menubar->GetFileMenu());
#endif

#if wxUSE_TOOLBAR
	this->toolbar = new QemuUIToolBar(this);
	SetToolBar(this->toolbar);
#endif

	this->mainwindow = new wxScrolledWindow(this);
	mainwindow->SetScrollRate(10,10);
	mainwindow->SetScrollbars(20, 20, 50, 50);

#if wxUSE_STATUSBAR
	this->statusbar = new QemuUIStatusBar(this);
	SetStatusBar(this->statusbar);
#endif
    
	this->notebook = new QemuUINotebook(mainwindow);
	this->CreateHomepage();
	UpdateState();
	this->DoLayout();
}

void QemuMainFrame::DoLayout(){
	wxBoxSizer *notebookSizer = new wxBoxSizer(wxHORIZONTAL );
	notebookSizer->Add(notebook,1,wxEXPAND,0);
	mainwindow->SetAutoLayout(true);
	mainwindow->SetSizer(notebookSizer);
	notebookSizer->Fit(mainwindow);
	notebookSizer->SetSizeHints(mainwindow);
	notebookSizer->Layout();

	wxBoxSizer *frameSizer = new wxBoxSizer(wxVERTICAL);
	frameSizer->Add(mainwindow,1,wxEXPAND,0);
	frameSizer->Fit(this);
	frameSizer->SetSizeHints(this);
	
	SetAutoLayout(true);
	SetSizer(frameSizer);
	Layout();
	SetSize(800,700);
	Centre();
}

void QemuMainFrame::CreateHomepage(){
	notebook->CreateHomepage();
	menubar->PrependWindowMenuEntry(_("Homepage"));
}

void QemuMainFrame::OnQuitCommandEvent(wxCommandEvent& WXUNUSED(event)){
	Close();
}

void QemuMainFrame::OnQuit(wxCloseEvent& event)
{
#if wxUSE_MEMORY_TRACING
	wxDebugContext::SetFile(wxT("log.txt"));
	wxDebugContext::Dump();
	wxDebugContext::PrintStatistics();
#endif
	for(int i = 0; i < vms.GetCount(); i++){
		if(vms.Item(i)->IsRunning()){
			if(wxMessageBox(
				_("Some virtual machines are still running, "
				  "are you sure you wan't to close QEMU?"),
				_("Warning"),
				wxICON_WARNING | wxYES_NO,
				this
			) == wxNO){
				event.Veto();
				return;
			};
		}
	}

	Destroy();
}

void QemuMainFrame::OnOpenQemuDoc(wxCommandEvent& WXUNUSED(event))
{
	if(!wxLaunchDefaultBrowser(QEMU_GUI_DOC)){
		wxLogError(_("Could not open qemu documentation."));
	}
}

void QemuMainFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{

/*
  wxTextCtrl* contentText = new wxTextCtrl;
    contentText->Create( mainwindow, wxID_ANY, wxT("TEST TEST http://www.wxwidgets.org/"), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_AUTO_URL|wxTE_CENTRE|wxTE_WORDWRAP|wxTE_READONLY);
*/
		
	wxString msg;
	msg << _("QEMU GUI version");
	msg << wxT(" "QEMU_GUI_VERSION"\n");
	msg << QEMU_GUI_BUILD_INFO;
	msg << _("\n\nThis is free software licensed under the GNU GPL.\n");
	msg << _("(c) 2006 Marc Tanner\n");
	msg << _("<"QEMU_GUI_MAIL">");

	wxMessageBox(msg, _("About QEMU GUI"), wxOK | wxICON_INFORMATION, this);
}


void QemuMainFrame::OnToggleStatusBar(wxCommandEvent& event)
{
	if(event.IsChecked())
		statusbar->Show();
	else 
    		statusbar->Hide();

	PositionStatusBar();
}

void QemuMainFrame::OnToggleToolBar(wxCommandEvent& event){
	if(event.IsChecked() && !this->toolbar){
		this->toolbar = new QemuUIToolBar(this);
		SetToolBar(this->toolbar);
		if(this->toolbarStyle != 0){
			toolbar->SetWindowStyle(toolbarStyle);
			toolbar->Layout();
		}
	} else {
		this->toolbarStyle = toolbar->GetWindowStyle();
		delete this->toolbar;
		this->toolbar = NULL;
		SetToolBar(NULL);
	}
	menubar->EnableToolbar(event.IsChecked());
}

void QemuMainFrame::OnToolBarSettingChange(wxCommandEvent& event){
 	long style = wxTB_HORIZONTAL | wxNO_BORDER | wxTB_FLAT;
	switch(event.GetId()){
		case QEMU_TOOLBAR_TEXT:
			style |= wxTB_TEXT | wxTB_NOICONS;
			break;
		case QEMU_TOOLBAR_ICONS:
			break;
		case QEMU_TOOLBAR_BOTH:
			style |= wxTB_TEXT;
			break;
	}

	toolbar->SetWindowStyle(style);
	toolbar->Layout();
}

void QemuMainFrame::OnFullScreen(wxCommandEvent &WXUNUSED(event)){
	ShowFullScreen(!IsFullScreen());
}

void QemuMainFrame::OnShowHomepage(wxCommandEvent &WXUNUSED(event)){
	ShowHomepage();
}

void QemuMainFrame::ShowHomepage(){
	if(notebook->HasHomepage()){
		const int pos = notebook->ShowHomepage();
		menubar->CheckWindowMenuEntry(pos);
	} else {
		CreateHomepage();
	}

	toolbar->Disable();
	statusbar->Disable();
}

void QemuMainFrame::OnChangeWindow(wxCommandEvent& event){
	notebook->SetSelection(menubar->GetCheckedWindowMenuEntry());
}

void QemuMainFrame::OnNextWindow(wxCommandEvent& WXUNUSED(event)){
	notebook->AdvanceSelection();
}

void QemuMainFrame::OnPrevWindow(wxCommandEvent& WXUNUSED(event)){
	notebook->AdvanceSelection(false);
}

void QemuMainFrame::OnNewVM(wxCommandEvent& WXUNUSED(event)){
	QemuUINewVMWizard *wizard = new QemuUINewVMWizard(this);
	QemuVM *vm = wizard->RunIt();
	wizard->Destroy();
	if(vm){
		OpenVM(vm);
	}
}

void QemuMainFrame::OnOpenVM(wxCommandEvent& WXUNUSED(event)){
	wxFileDialog *dialog = new wxFileDialog(
		this,
		_("Select a VM configuration file to open"),
		wxEmptyString,wxEmptyString,
		_("VM configuration (*.qvmc)|*.qvmc")
	);
	
	dialog->SetDirectory(wxGetHomeDir());
	dialog->CentreOnParent();

	if(dialog->ShowModal() == wxID_OK){
		OpenVM(dialog->GetPath());
	}

	dialog->Destroy();
}

void QemuMainFrame::OnReOpenVM(wxCommandEvent& event){
	OpenVM(filehistory->GetHistoryFile(event.GetId() - wxID_FILE1));
}

void QemuMainFrame::OpenVM(const wxString& config){
	QemuVM *vm = new QemuVM(config);
	if(!OpenVM(vm)){
		delete vm;
	}
}

bool QemuMainFrame::OpenVM(QemuVM *vm){
	const wxString config = vm->GetConfigFile();
	for(int i = 0; i < vms.GetCount(); i++){
		if(vms.Item(i)->GetConfigFile().Cmp(config) == 0){
			wxMessageBox(
				_("This virutal machine is already open."),
				_("Error"),
				wxICON_ERROR | wxOK,
				this
			);
			return false;
		}
	}

	filehistory->AddFileToHistory(config);
	menubar->UpdateFileHistory(filehistory->GetCount());
	// it is important that the window menu entry gets created
	// before a new notebook page is added, because within the 
	// OnNotebookChange event handler the new item will be checked.
	menubar->AppendWindowMenuEntry(vm->GetTitle());
	notebook->AddVM(vm);

	// add to the array of open vm's
	vms.Add(vm);

	return true;
}

QemuUINotebookPage* QemuMainFrame::GetCurrentVMTab(){
	return notebook->GetCurrentVMTab();
}

QemuVM* QemuMainFrame::GetCurrentVM(){
	return notebook->GetCurrentVM();
}

void QemuMainFrame::OnVMSettings(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(!vm)
		return;

        QemuVMConfigDialog *dialog = new QemuVMConfigDialog(this,vm);
	if(dialog->ShowModal() == wxID_OK){
		dialog->Save();
	}			
}

void QemuMainFrame::OnVMSendCtrlAltDel(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(!vm || !vm->IsRunning())
		return;
	vm->SendKey(wxT("ctrl-alt-delete"));
}

void QemuMainFrame::OnPowerOnVM(wxCommandEvent& WXUNUSED(event)){
	QemuUINotebookPage *tab = GetCurrentVMTab();
	QemuVM *vm = tab->GetVM();
	if(tab && vm){
		bool paused = vm->IsPaused();
		if(vm->PowerOn() < 0){
			wxMessageBox(_("Could not start qemu"),_("Error"),
			             wxICON_ERROR | wxOK, this);
			return;
		}
	
#if 1
		if(!paused){
			// give qemu some time to set up it's vnc server
			//wxMilliSleep(1000);
			if(tab->GetScreen()->Connect(vm->GetCurrentVNCDisplay())){
				vm->AddEventHandler(tab->GetScreen());
				tab->ShowScreen();
			} else {
				wxLogError(_("Could not connect to vnc display."));
			}
		}
#endif
		UpdateState();
	}
}


void QemuMainFrame::OnPauseVM(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(vm){
		vm->Pause();
		UpdateState();
	}
}

void QemuMainFrame::OnPowerOffVM(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(vm){
		vm->PowerOff();
		UpdateState();
	}
}

void QemuMainFrame::OnResetVM(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(vm){
		vm->Reset();
	}
}

void QemuMainFrame::OnKillVM(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(vm){
		if(wxMessageBox(_("Do you really want to kill this VM?"), 
		   _("Warning"),wxICON_QUESTION | wxYES_NO, this) == wxYES){
			vm->Kill();
		}
	}
}

void QemuMainFrame::OnSnapshot(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(vm){
		vm->Snapshot();
		UpdateState();
	}
}

void QemuMainFrame::OnRevertSnapshot(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(vm){
		vm->Revert();
		UpdateState();
	}
}

void QemuMainFrame::OnToggleDisplay(wxCommandEvent& event){
	QemuUINotebookPage* tab = notebook->GetCurrentVMTab();
	switch(event.GetId()){
		case QEMU_TOOLBAR_DISPLAY_INFO:
			toolbar->ShowInfo();
			tab->ShowInfo();
			break;
		case QEMU_TOOLBAR_DISPLAY_SCREEN:
			toolbar->ShowScreen();
			tab->ShowScreen();
			break;
		case QEMU_TOOLBAR_DISPLAY_CONSOLE:
			toolbar->ShowConsole();
			tab->ShowConsole();
			break;
	}
}

void QemuMainFrame::OnSendCommandToVM(wxCommandEvent& WXUNUSED(event)){
	QemuVM *vm = GetCurrentVM();
	if(!vm) return;
	wxTextEntryDialog dialog(this,
		_T("Please enter a command which will be send to the qemu monitor."),
		_T("Please enter a command"),
		_T(""),
		wxOK | wxCANCEL
	);

	if (dialog.ShowModal() == wxID_OK){
        	wxMessageBox(vm->SendCommand(dialog.GetValue(),true), wxT("Response"), wxOK | wxICON_INFORMATION, this);
		//wxLogError(vm->SendCommand(dialog.GetValue()));
	}
}

void QemuMainFrame::OnNotebookPageChanged(wxNotebookEvent& event){
	const size_t pos = event.GetSelection();
	menubar->CheckWindowMenuEntry(pos);
	UpdateState();
}

void QemuMainFrame::CloseNotebookPage(const int pos){
	QemuVM *vm = GetCurrentVM();
	
	if(!vm){
		notebook->CloseHomepage();
		menubar->RemoveWindowMenuEntry(pos);
		return;
	}

	if(vm->IsRunning() && 
	   wxMessageBox(_("Do you really want to close this VM?"),_("Question"),
                        wxICON_QUESTION | wxYES_NO, this) != wxYES){
		return;
	}

	wxNotebookPage *page = notebook->GetCurrentPage();
	if(page){
		vm->PowerOff();
		vms.Remove(vm);
		delete vm;
		menubar->RemoveWindowMenuEntry(pos);
		notebook->RemovePage(pos);
		delete page;
		UpdateState();
	}
}

void QemuMainFrame::OnNotebookPageClosed(wxCommandEvent& WXUNUSED(event)){
	CloseNotebookPage(notebook->GetSelection());
}

void QemuMainFrame::UpdateState(){
	QemuUINotebookPage *tab = GetCurrentVMTab();

	if(!tab){
		menubar->Disable();
		toolbar->Disable();
		statusbar->Disable();
		return;
	}

	QemuVM *vm = tab->GetVM();
	menubar->Settings(!vm->IsRunning());
	menubar->SendCtrlAltDel(vm->IsRunning());
	menubar->PowerOn(!vm->IsRunning());
	menubar->PowerOff(vm->IsRunning() || vm->IsPaused());
	menubar->Pause(vm->IsRunning());
	menubar->Reset(vm->IsRunning() || vm->IsPaused());
	toolbar->PowerOn(!vm->IsRunning());
	toolbar->PowerOff(vm->IsRunning() || vm->IsPaused());
	toolbar->Pause(vm->IsRunning());
	toolbar->Reset(vm->IsRunning() || vm->IsPaused());
	toolbar->Kill(vm->IsRunning());
	toolbar->Snapshot(vm->IsRunning());
	toolbar->RevertSnapshot(vm->GetPID() > 0 && vm->HasSnapshot());
	toolbar->EnableDisplayArea();
	toolbar->EnableDisplayScreen(!vm->IsPoweredOff());
	toolbar->SendVMCommand(!vm->IsPoweredOff());

	if(tab->IsConsoleActive())
		toolbar->ShowConsole();
	else if(tab->IsScreenActive())
		toolbar->ShowScreen();
	else
		toolbar->ShowInfo();
	
	tab->GetConsole()->SetState(!vm->IsPoweredOff());

	if(vm->IsRunning())
		statusbar->Enable();
	else
		statusbar->Disable();
}

void QemuMainFrame::OnConnectCdRom(wxCommandEvent& event){
	GetCurrentVM()->ConnectCdRom(event.IsChecked());
}

void QemuMainFrame::SaveFileHistory(wxConfigBase *config){
	//config->SetPath(wxT("/filehistory"));
	filehistory->Save(*config);
	//config->SetPath(wxT("/"));
}

void QemuMainFrame::LoadFileHistory(wxConfigBase *config){
	//config->SetPath(wxT("/filehistory"));
	filehistory->Load(*config);
	menubar->UpdateFileHistory(filehistory->GetCount());
	//config->SetPath(wxT("/"));
}
