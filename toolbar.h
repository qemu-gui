#ifndef QEMU_GUI_TOOLBAR_H
#define QEMU_GUI_TOOLBAR_H

#include <wx/toolbar.h>

class QemuUIToolBar : public wxToolBar
{
public:
	QemuUIToolBar(wxWindow *parent,long style = wxTB_HORIZONTAL | 
	                               wxNO_BORDER | wxTB_TEXT | wxTB_FLAT);
	~QemuUIToolBar();
	/**
	 * Disable all items in the toolbar
	 */
	void Disable();
	/**
	 * Enable/Disable all display related toolbar items
	 */
	void EnableDisplayArea();
	void DisableDisplayArea();
	/**
	 * Set the state of the various toolbar buttons
	 */
	void PowerOn(bool state);
	void PowerOff(bool state);
	void Pause(bool state);
	void Reset(bool state);
	void Kill(bool state);
	void Snapshot(bool state);
	void RevertSnapshot(bool state);
	void ShowInfo();
	void ShowScreen();
	void ShowConsole();
	void SendVMCommand(bool state);
	void EnableDisplayScreen(bool state);
};

#endif
