#ifndef QEMU_GUI_VM_CONFIG_DIALOG
#define QEMU_GUI_VM_CONFIG_DIALOG

#include <wx/propdlg.h>
#include <wx/generic/propdlg.h>

class QemuVM;
class wxTextCtrl;
class wxSpinCtrl;
class wxCheckBox;
class wxRadioButton;
class wxPanel;
class QemuDeviceWidget;
class QemuDirectoryWidget;

class QemuVMConfigDialog : public wxPropertySheetDialog {
public:
	QemuVMConfigDialog(wxWindow *parent,QemuVM *vm);
	~QemuVMConfigDialog();
	void Save();
private:
	wxPanel* CreateGeneralPage(wxWindow *parent);
	wxPanel* CreateDevicePage(wxWindow *parent);
	wxPanel* CreateNetworkPage(wxWindow *parent);
	wxPanel* CreateMiscPage(wxWindow *parent);
	void Load();
	QemuVM *vm;
	wxTextCtrl *title;
	QemuDirectoryWidget *dir;
	wxSpinCtrl *smp;
	wxSpinCtrl *memory;
	QemuDeviceWidget *hda;
	QemuDeviceWidget *hdb;
	QemuDeviceWidget *hdc;
	QemuDeviceWidget *hdd;
	QemuDeviceWidget *cdrom;
	QemuDeviceWidget *fda;
	QemuDeviceWidget *fdb;
	QemuDirectoryWidget *smb;
	QemuDirectoryWidget *tftp;
	wxRadioButton *boota;
	wxRadioButton *bootc;
	wxRadioButton *bootd;
	wxChoice *keyboard;
	wxCheckBox *localtime;
	wxCheckBox *win2khack;
	wxCheckBox *stdvga;
	wxCheckBox *noacpi;
	wxCheckBox *snapshotmode;
	wxSpinCtrl *vncdisplay;
	wxTextCtrl *cmdlineargs;
	static const wxString keyboardLayouts[];
};

#endif
