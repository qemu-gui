#include <wx/intl.h>
#include <wx/menu.h>
#include "menubar.h"
#include "events.h"
#include "mainframe.h"

QemuUIMenuBar::QemuUIMenuBar() : wxMenuBar(){
	this->Append(CreateFileMenu(),  _("&File"));
	this->Append(CreateViewMenu(),  _("&View"));
	this->Append(CreateVMMenu(),    _("V&M"));
	this->Append(CreatePowerMenu(), _("&Power"));
	this->Append(CreateWindowMenu(),_("&Windows"));
	this->Append(CreateHelpMenu(),  _("&Help"));
}

wxMenu *QemuUIMenuBar::CreateFileMenu(){
	this->filemenu = new wxMenu();
	
	filemenu->Append(
		QEMU_NEW,
		_("&N&ew\tCtrl-N"),
		_("Create a new VM")
	);
	
	filemenu->Append(
		QEMU_OPEN,
		_("&Open\tCtrl-O"),
		_("Open an existing VM")
	);
	
	filemenu->Append(
		QEMU_CLOSE,
		_("&Close\tCtrl-Q"),
		_("Close this VM")
	);
	
	filemenu->AppendSeparator();
	
	filemenu->Append(
		QEMU_QUIT,
		_("E&xit\tCtrl-X"),
		_("Quit this program")
	);
	
	return this->filemenu;
}

wxMenu *QemuUIMenuBar::CreateViewMenu(){
	this->viewmenu = new wxMenu();
	
	viewmenu->Append(
		QEMU_HOMEPAGE,
		_("&Homepage"),
		_("Show start screen")
	);
	
	viewmenu->AppendCheckItem(
		QEMU_FULLSCREEN,
		_("&Fullscreen"),
		_("Toggle fullscreen mode")
	);
	
	wxMenu *toolbarmenu = new wxMenu();
	toolbarmenu->AppendCheckItem(
		QEMU_TOGGLE_TOOLBAR,
		_("&Toolbar"),
		_("Toggle toolbar")
	);
	toolbarmenu->Check(QEMU_TOGGLE_TOOLBAR,true);
	toolbarmenu->AppendSeparator();
	toolbarmenu->AppendRadioItem(QEMU_TOOLBAR_ICONS,_("Icons"));
	toolbarmenu->AppendRadioItem(QEMU_TOOLBAR_TEXT,_("Text"));
	toolbarmenu->AppendRadioItem(QEMU_TOOLBAR_BOTH,_("Both"));
	toolbarmenu->Check(QEMU_TOOLBAR_BOTH,true);

	viewmenu->Append(
		wxID_ANY,
		_("Toolbar"),
		toolbarmenu,
		_("Toolbar related settings")
	);
	
	viewmenu->AppendCheckItem(
		QEMU_TOGGLE_STATUSBAR,
		_("&Status Bar"),
		_("Toggle status bar")
	);
	viewmenu->Check(QEMU_TOGGLE_STATUSBAR,true);
	
	return this->viewmenu;
}

wxMenu* QemuUIMenuBar::CreateVMMenu(){
	this->vmmenu = new wxMenu();
	vmmenu->Append(QEMU_VM_SEND_CTRL_ALT_DEL,_("Send Ctrl+Alt+Del"));
	vmmenu->Append(QEMU_VM_SETTINGS,_("Settings"));
	return this->vmmenu;
}


wxMenu* QemuUIMenuBar::CreatePowerMenu(){
	this->powermenu = new wxMenu();
	powermenu->Append(QEMU_VM_START,_("Power on"));
	powermenu->Append(QEMU_VM_SHUTDOWN,_("Power off"));
	powermenu->Append(QEMU_VM_PAUSE,_("Pause"));
	powermenu->Append(QEMU_VM_RESET,_("Reset"));
	return this->powermenu;
}

wxMenu* QemuUIMenuBar::CreateWindowMenu(){
	this->windowmenu = new wxMenu();
	windowmenu->AppendSeparator();
	windowmenu->Append(
		QEMU_WINDOW_NEXT,
		_("&Next Window\tCtrl+TAB")
	);
	windowmenu->Append(
		QEMU_WINDOW_PREV,
		_("&Previous Window\tCtrl+Shift+Tab")
	);
	return this->windowmenu;
}

wxMenu *QemuUIMenuBar::CreateHelpMenu(){
	this->helpmenu = new wxMenu();
	helpmenu->Append(
		QEMU_DOC,
		_("&Documentation\tF1"),
		_("Show Qemu documentation")
	);
	helpmenu->Append(
		QEMU_ABOUT,
		_("&About"),
		_("Show about dialog")
	);
	return this->helpmenu;
}

void QemuUIMenuBar::Disable(){
	powermenu->Enable(QEMU_VM_START,false);
	powermenu->Enable(QEMU_VM_PAUSE,false);
	powermenu->Enable(QEMU_VM_SHUTDOWN,false);
	powermenu->Enable(QEMU_VM_RESET,false);
	vmmenu->Enable(QEMU_VM_SETTINGS,false);
	vmmenu->Enable(QEMU_VM_SEND_CTRL_ALT_DEL,false);
}

void QemuUIMenuBar::Settings(bool state){
	vmmenu->Enable(QEMU_VM_SETTINGS,state);
}

void QemuUIMenuBar::SendCtrlAltDel(bool state){
	vmmenu->Enable(QEMU_VM_SEND_CTRL_ALT_DEL,state);
}

void QemuUIMenuBar::PowerOn(bool state){
	powermenu->Enable(QEMU_VM_START,state);
}

void QemuUIMenuBar::Pause(bool state){
	powermenu->Enable(QEMU_VM_PAUSE,state);
}

void QemuUIMenuBar::PowerOff(bool state){
	powermenu->Enable(QEMU_VM_SHUTDOWN,state);
}

void QemuUIMenuBar::Reset(bool state){
	powermenu->Enable(QEMU_VM_RESET,state);
}

void QemuUIMenuBar::EnableToolbar(bool value){
	viewmenu->Enable(QEMU_TOOLBAR_TEXT,value);
	viewmenu->Enable(QEMU_TOOLBAR_ICONS,value);
	viewmenu->Enable(QEMU_TOOLBAR_BOTH,value);
}

void QemuUIMenuBar::CheckFavorites(bool value){
	viewmenu->Check(QEMU_SPLIT,value);
}

void QemuUIMenuBar::UpdateWindowMenu(const size_t posOld,const size_t posNew){
	wxMenuItem *item = windowmenu->FindItemByPosition(posOld);
	if(item){
		windowmenu->Remove(item);
		windowmenu->Insert(posNew,item);
	}
}

void QemuUIMenuBar::RemoveWindowMenuEntry(const size_t pos){
	wxMenuItem *item = windowmenu->FindItemByPosition(pos);
	if(item){
		windowmenu->Remove(item);
		delete item;
		CheckWindowMenuEntry(pos > 0 ? pos - 1 : 0);
	}
}

void QemuUIMenuBar::InsertWindowMenuEntry(const size_t pos,const wxString& title){
	/* there seem to be a problem in wxwidgets which occurs
	 * when radio items are removed and then later back added
	 * so this is an ugly workaround. All items are removed
	 * and then back added.
	 * 
	 * see also my post to wx-dev
	 * http://lists.wxwidgets.org/cgi-bin/ezmlm-cgi?5:mss:76732:200608:mfgbpjnmoondkdinmimb 
	 */ 
	const int count = windowmenu->GetMenuItemCount();
	wxMenuItem *tmp[count];
	for(int i = count - 1; i >= 0; i--){
		tmp[i] = windowmenu->FindItemByPosition(i);
		windowmenu->Remove(tmp[i]);
	}
/*	
	wxMenuItem *workaround = windowmenu->Append(-1,wxEmptyString);
	windowmenu->Remove(workaround);
	delete workaround;
*/
	const int id = wxNewId();
	wxMenuItem *item = new wxMenuItem(windowmenu,id,title,wxEmptyString,
	                                  wxITEM_RADIO);
	// this evalutes to false on windows, and thus the menuentries do not
	// respond to user events.
	if(GetParent()){
		GetParent()->Connect(
			id,
			wxEVT_COMMAND_MENU_SELECTED,
			wxCommandEventHandler(QemuMainFrame::OnChangeWindow)
		);
	}

	for(int i = 0; i < count; i++){
		if(i == pos){
			windowmenu->Append(item);
		}
		windowmenu->Append(tmp[i]);
	}
}

void QemuUIMenuBar::AppendWindowMenuEntry(const wxString& title){
	// subtract 3 because of the separator and 
	// the 2 standard menu items
	const int pos = windowmenu->GetMenuItemCount() - 3;
	InsertWindowMenuEntry(pos,title);
}

void QemuUIMenuBar::PrependWindowMenuEntry(const wxString& title){
	InsertWindowMenuEntry(0,title);
}

void QemuUIMenuBar::CheckWindowMenuEntry(size_t pos){
	// return if there are no entries available
	// check for 3 because of the separator and the 
	// 2 standard menu items which are allways there
	const size_t count = windowmenu->GetMenuItemCount();
	
	if(count <= 3)
		return;
	
	wxMenuItem *item = windowmenu->FindItemByPosition(pos);
	if(item)
		item->Check(true);
}

int QemuUIMenuBar::GetCheckedWindowMenuEntry(){
	for(int i = 0; i < windowmenu->GetMenuItemCount() - 3; i++){
		wxMenuItem *item = windowmenu->FindItemByPosition(i);
		if(item->IsChecked())
			return  i;
	}
}

wxMenu* QemuUIMenuBar::GetFileMenu(){
	return this->filemenu;
}

void QemuUIMenuBar::UpdateFileHistory(const size_t fileHistoryCount){
	wxMenuItem *separator = NULL; 
	for(int i = filemenu->GetMenuItemCount() - 1; i >= 0; i--){
		separator = filemenu->FindItemByPosition(i);
		if(separator->GetKind() == wxITEM_SEPARATOR){
			filemenu->Remove(separator);
		}
	}
	if(fileHistoryCount > 0){
		filemenu->InsertSeparator(filemenu->GetMenuItemCount() - fileHistoryCount - 1);
	}
	wxMenuItem *exit = filemenu->Remove(QEMU_QUIT);
	filemenu->AppendSeparator();
	filemenu->Append(exit);
}
