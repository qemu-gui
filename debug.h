#ifndef QEMU_GUI_DEBUG_H
#define QEMU_GUI_DEBUG_H

#include <stdio.h>

#ifdef DEBUG_ALL_FLAG
	#define DEBUG_MOUSE_FLAG
	#define DEBUG_KEYBBOARD_FLAG
	#define DEBUG_VNC_FLAG
	#define DEBUG_PAINT_FLAG
#endif 

#define DEBUG(format,args...) \
	fprintf(stderr,format"\n",## args)

#define DEBUG_SECTION(title,format,args...) \
	fprintf(stderr,title": "format"\n",## args)

#ifdef DEBUG_MOUSE_FLAG
	#define DEBUG_MOUSE(format,args...) \
		DEBUG_SECTION("mouse",format,## args)
#else
	#define DEBUG_MOUSE(format,args...) \
		do {} while(0)
#endif

#ifdef DEBUG_KEYBOARD_FLAG
	#define DEBUG_KEYBOARD(format,args...) \
		DEBUG_SECTION("keyboard",format,## args)
#else
	#define DEBUG_KEYBOARD(format,args...) \
		do {} while(0)
#endif

#ifdef DEBUG_VNC_FLAG
	#define DEBUG_VNC(format,args...) \
		DEBUG_SECTION("vnc",format,## args)
#else
	#define DEBUG_VNC(format,args...) \
		do {} while(0)
#endif

#ifdef DEBUG_PAINT_FLAG
	#define DEBUG_PAINT(format,args...) \
		DEBUG_SECTION("paint",format,## args)
#else
	#define DEBUG_PAINT(format,args...) \
		do {} while(0)
#endif

#endif
