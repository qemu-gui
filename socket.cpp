#include <wx/socket.h>
#include <wx/log.h>
#include "socket.h"
#include "events.h"

#if defined(__WXMSW__)
	#define EOL wxT("\r\n \r\n")
#else
	#define EOL wxT("\n")
#endif

DEFINE_EVENT_TYPE(EVT_SOCKET_INPUT)
DEFINE_EVENT_TYPE(EVT_SOCKET_WRITE)
DEFINE_EVENT_TYPE(EVT_SOCKET_LOST)

BEGIN_EVENT_TABLE(MonitorSocket, wxEvtHandler)
	EVT_SOCKET(wxID_ANY, MonitorSocket::OnSocketEvent)
END_EVENT_TABLE()

MonitorSocket::MonitorSocket() : wxEvtHandler(){
}

bool MonitorSocket::Connect(const wxString& host, const unsigned int port){
	wxIPV4address address;
	address.Hostname(host);
	address.Service(port);
	socket = new wxSocketClient();
	socket->SetEventHandler(*this, wxID_ANY);
	socket->SetFlags(wxSOCKET_NOWAIT);
	socket->SetNotify(wxSOCKET_CONNECTION_FLAG |
	                  wxSOCKET_INPUT_FLAG |
	                  wxSOCKET_LOST_FLAG);
	socket->Notify(true);    
	bool state = socket->Connect(address,true /* wait and block */);
	if(!state)
		socket->Close();
	return state;
}

void MonitorSocket::Disconnect(){
	socket->Destroy();
}

void MonitorSocket::OnSocketEvent(wxSocketEvent& event){
	switch(event.GetSocketEvent()){
		case wxSOCKET_INPUT: {
			wxString buffer;
	
			while(socket->IsData()){
				buffer.Append(Read());
			}
	
			if(buffer.IsEmpty())
				return;

			data.Append(buffer);
			wxCommandEvent event(EVT_SOCKET_INPUT,wxID_ANY);
			event.SetEventObject(this);
			event.SetString(buffer);
			SendEvent(event);
			break;
		}
		case wxSOCKET_LOST:
			break;
		case wxSOCKET_CONNECTION: 
			break;
  	}
}

void MonitorSocket::Write(const wxString& cmd, bool block){
	if(block)
		data.Clear();
	
	wxString tmp = cmd + EOL;
	socket->Write(tmp.fn_str(),tmp.Len());

	wxCommandEvent event(EVT_SOCKET_WRITE,wxID_ANY);
	event.SetEventObject(this);
	event.SetString(cmd);
	SendEvent(event);
}

wxString MonitorSocket::Read(){
	char buf[1024];
	socket->Read(buf, 1024);
	buf[socket->LastCount()] = '\0';

	wxString buffer(buf, wxConvUTF8);
	int found = 0;
	do {
		found = buffer.Find(wxT("[K"));
		if(found > 0)
			buffer.Remove(0,found + 2);
	} while (found > 0);
	//buffer.Replace(wxT("\r\n"),wxT(""));
	buffer.Trim().Trim(false);
	return buffer;
}

wxString MonitorSocket::ReadAll(){
	int found;
	do {
		socket->WaitForRead(0,250);
	} while((found = data.Find(wxT("(qemu)"))) < 0);
	
	wxString tmp = data.Left(found);
	// skip "(qemu)"
	data.Remove(0,found + 6);
	return tmp.Trim();
}


void MonitorSocket::AddEventHandler(wxEvtHandler *handler){
	evtHandlers.Add(handler);
}

void MonitorSocket::SendEvent(wxCommandEvent& event){
	for(int i = 0; i < evtHandlers.GetCount(); i++){
		evtHandlers.Item(i)->ProcessEvent(event);
	}
}
