#ifndef QEMU_GUI_CONFIG_H
#define QEMU_GUI_CONFIG_H

#define APP_VENDOR	 _T("Qemu GUI development team")
#define APP_NAME	 _T("Qemu GUI")
#define QEMU_GUI_VERSION _T("0.01")
#define QEMU_GUI_URL	 _T("http://www.braind-dump.org/projects/qemu-gui/")
#define QEMU_GUI_MAIL	 _T("mat@brain-dump.org")
#define QEMU_GUI_DOC	 _T("www.qemu.org/qemu-doc.html")

#if defined(__WXMSW__)
	#define APP_PLATFORM _T("Windows")
#elif defined(__WXOS2__)
	#define APP_PLATFORM _T("OS/2")
#elif defined(__WXMAC__)
	#define APP_PLATFORM _T("Mac OS/X")
#elif defined(__LINUX__)
	#define APP_PLATFORM _T("Linux")
#elif defined(__FREEBSD__)
	#define APP_PLATFORM _T("FreeBSD")
#elif defined(__BSD__)
	#define APP_PLATFORM _T("BSD")
#elif defined(__SOLARIS__)
	#define APP_PLATFORM _T("Solaris")
#elif defined(__UNIX__)
	#define APP_PLATFORM _T("Unix")
#else
	#define APP_PLATFORM _T("Unknown")
#endif

#if wxUSE_UNICODE
	#define APP_WXANSI_UNICODE wxT("unicode")
#else
	#define APP_WXANSI_UNICODE wxT("ANSI")
#endif

#ifdef __WXDEBUG__
	#define APP_DEBUG_MODE wxT(", debug build")
#else
	#define APP_DEBUG_MODE wxT("")
#endif

#define QEMU_GUI_BUILD_INFO	(wxString(wxT(__DATE__)) + wxT(", ") + wxT(__TIME__) + \
	wxT(" - wx") + wxT(wxVERSION_NUM_DOT_STRING) + wxT(" (") + APP_PLATFORM + \
	wxT(", ") + APP_WXANSI_UNICODE + APP_DEBUG_MODE + wxT(")") )

#define QEMU_GUI_USE_VNC

#endif
