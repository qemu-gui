#
# Makefile for qemu-gui a graphical frontend for qemu
#   (c) 2006 Marc Andr� Tanner <mat@brain-dump.org>
#

WX_CONFIG  = wx-config

WX_FLAGS  = `$(WX_CONFIG) --cxxflags`

CXX = $(shell $(WX_CONFIG) --cxx)

CXXFLAGS = 
CXXDEBUGFLAGS = -ggdb -g -O0 -pg -D__WXDEBUG__ 
#CXXDEBUGFLAGS = -ggdb -g -O0 -pg 
LDFLAGS = 
#LDFLAGS = -L/usr/local/lib
LDDEBUGFLAGS = -pg

PROGRAM = qemu-ui

OBJECTS = $(PROGRAM).o \
          mainframe.o \
          menubar.o \
          toolbar.o \
          statusbar.o \
          wizard.o \
          notebook.o \
          console.o \
          pipedprocess.o \
          socket.o \
          vm.o \
          favorites.o \
          vmdialog.o \
          guibuilder.o \
          vncpanel.o \
          vncdisplaythread.o

LIBS = `$(WX_CONFIG) --libs` -lvncclient 

ifeq ($(MAKECMDGOALS),debug)
	CXXFLAGS += $(CXXDEBUGFLAGS)
	LDFLAGS  += $(LDDEBUGFLAGS)
	LIBS = `$(WX_CONFIG) --libs --debug=yes` -lvncclient 
endif

ifdef WINDOWS
	LIBS += -lws2_32
endif


.SUFFIXES:	.o .cpp

.cpp.o :
	$(CXX) $(CXXFLAGS) -c $(WX_FLAGS) -o $@ $<

default:$(PROGRAM)

debug:	$(PROGRAM)

$(PROGRAM):	$(OBJECTS) 
	$(CXX) -o $(PROGRAM) $(OBJECTS) $(LIBS) $(LDFLAGS)

clean: 
	rm -f *.o $(PROGRAM)

help:
	@echo 'Available targets'
	@echo '  - default [WINDOWS=y]'
	@echo '      Yust type make on *nix systems or make WINDOWS=y '
	@echo '      on Windows systems to compile everything'
	@echo '  - debug'
	@echo '      Build with debuging flags enabled'
	@echo '  - install [PREFIX=/usr/local]'
	@echo '      Installs the files as specified with PREFIX'
	@echo '  - clean'
	@echo '      Remove all compiled objects'
