#include "vncdisplaythread.h"
#include "vncpanel.h"

/* inline */ VNCDisplayThread::VNCDisplayThread(VNCPanel *panel) 
                              :wxThread(wxTHREAD_JOINABLE),
                               panel(panel),
                               running(true){}
                                                       
/* inline */ void VNCDisplayThread::Stop() { running = false; }

void *VNCDisplayThread::Entry() {
	rfbClient *cl = panel->GetVNCClient();
	if(!cl)
		return NULL;	
	
	while(running){
		int i = WaitForMessage(cl,500);
		if(i<0)
			return NULL;
		if(i){
			wxMutexGuiEnter();
	    		if(!HandleRFBServerMessage(cl)){
				wxMutexGuiLeave();
				return NULL;
			}
			wxMutexGuiLeave();
		}
	}
	
	return NULL;
}
